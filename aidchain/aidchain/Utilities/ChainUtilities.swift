//
//  ChainUtilities.swift
//  aidchain
//
//  Created by Denys Doronin on 8/23/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ChainUtilities {

    class func generateIDString()->String {
        return "Aid\(Int(Date().timeIntervalSince1970) % 1000000)"
    }
}
