//
//  MenuController.swift
//  Fraud-detection
//
//  Created by Doronin Denis on 5/24/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit

class AvailableAssetsController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lblDetails: UILabel!

    @IBOutlet weak var tableView: UITableView!
    
    private static let сellId = "assetCellId"
    
    static let detailsSegue = "segueDetails"
    var assetsModel: AvailableAssetsModel?
    var selectedAsset: ChainEntity.Type?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        self.lblDetails.text = "\(assetsModel?.targetRole.rawValue ?? "Unknown") assets"
        self.tableView.dataSource = self
        self.tableView.delegate = self
        // Do any additional setup after loading the view.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .done, target: self, action: #selector(actionLogout(_:)))
    }
    
    @objc func actionLogout(_ sender: Any) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.currentUser = nil
            print("User logout action")
        }
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // 1st section for assets, 2nd for transactions
        return 2
    }

    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0 {
            return assetsModel?.assetsToView?.count ?? 0;
        }
        if section == 1 {
            return assetsModel?.transactions?.count ?? 0;
        }
        
        return 0;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AvailableAssetsController.сellId, for: indexPath) as! RoleSelectionCell
    
        if indexPath.section == 0 {
            if let title = typeToAsset(type: assetsModel?.assetsToView?[indexPath.row] ?? ChainEntity.self) {
                cell.btnSelectRole.setTitle(title.rawValue, for: .normal)
            }
            cell.btnSelectRole.addTarget(self, action: #selector(assetSelected), for: .touchUpInside)
            cell.btnSelectRole.tag = indexPath.row
        }
        
        if indexPath.section == 1 {
            if let title = typeToAsset(type: assetsModel?.transactions?[indexPath.row] ?? ChainEntity.self) {
                cell.btnSelectRole.setTitle(title.rawValue, for: .normal)
            }
            cell.btnSelectRole.addTarget(self, action: #selector(transactionSelected), for: .touchUpInside)
            cell.btnSelectRole.tag = (assetsModel?.assetsToView?.count ?? 0) + indexPath.row
        }
        
        return cell
    }
    
    @objc func transactionSelected(sender: UIButton!) {
        self.selectedAsset = assetsModel?.transactions?[sender.tag - (assetsModel?.assetsToView?.count ?? 0)]
        self.performSegue(withIdentifier: AvailableAssetsController.detailsSegue, sender: self)
    }
    
    @objc func assetSelected(sender: UIButton!) {
        self.selectedAsset = assetsModel?.assetsToView?[sender.tag]
        self.performSegue(withIdentifier: AvailableAssetsController.detailsSegue, sender: self)
    }
    
    @IBAction func showDetails(_ sender: UIButton) {
        self.performSegue(withIdentifier: AvailableAssetsController.detailsSegue, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == AvailableAssetsController.detailsSegue {
            if let vc = segue.destination as? AssetsListController, let assetsModel = assetsModel {

                let listModel = AssetListModel(role: assetsModel.targetRole, assetType: self.selectedAsset!)
                vc.model = listModel
                
                if assetsModel.isAllowedToCreate(assetType: self.selectedAsset!) == false {
                    vc.navigationItem.rightBarButtonItem = nil
                }
                
                listModel.allowedToUpdate = assetsModel.isAllowedToUpdate(assetType: self.selectedAsset!)
            }
        }
    }

}
