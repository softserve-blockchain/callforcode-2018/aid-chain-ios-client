//
//  SetPasswordController.swift
//  aidchain
//
//  Created by Denys Doronin on 8/16/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class SetPasswordController: BaseViewController {

    @IBOutlet weak var lblSelectedRole: UILabel!
    
    @IBOutlet weak var tfLogin: UITextField!
    
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var tfConfirmPassword: UITextField!

    @IBOutlet weak var lblPasswordHint: UILabel!
    var entityId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Password setup"
    
        lblPasswordHint.text = NSLocalizedString("Password should be setup after registration to login into application. Please fill fields below.", comment: "")
        
        self.tfLogin.text = entityId
        tfPassword.delegate = self
        tfConfirmPassword.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validateInput() -> (Bool, String?) {
        guard let password = tfPassword.text, !password.isEmpty,
            let confirm = tfConfirmPassword.text, !confirm.isEmpty
            else { return (false, "Some fields are empty")}
        
        guard password == confirm else { return (false, "Passwords don't match") }
        
        return (true,nil)
    }
    
    @IBAction func setPasswordAction(_ sender: Any) {
        
        let (res,errorMessage) = validateInput()
        
        if res == false {
            showAlert("Validation warning",errorMessage)
            return
        }
        
        let sv = BaseViewController.displaySpinner(onView: self.view)
        
        let loginManager =  LoginManager.fetchLoginManager()
        if let entityId = entityId {
            loginManager.storedUsers?[entityId]?.password = self.tfPassword!.text
        }
        
        
      //  let status = Keychain.save(key: currentUserPassword, data: (self.tfPassword!.text?.data(using: .utf8))!)
        
        if LoginManager.saveLoginManager(loginManager: loginManager) {
        BaseViewController.removeSpinner(spinner: sv)
            self.showAlert("Register success", "Now you can use \(entityId!) as a login",{ action in
                self.dismiss(animated: true, completion: {
                })
            })
        }
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if tfConfirmPassword == textField {
            setPasswordAction(textField)
        }
        return super.textFieldShouldReturn(textField)
    }

}
