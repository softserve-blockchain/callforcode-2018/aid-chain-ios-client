//
//  CreateAssetController.swift
//  Fraud-detection
//
//  Created by Denys Doronin on 5/30/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit

class SaveAssetController: UITableViewController,UITextFieldDelegate {

    public var isUpdateScenario: Bool = false
    public var model: SaveAssetModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(actionDone(_:)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func actionDone(_ sender: Any) {
        populate()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.chainProperies.count
    }

    
    fileprivate func regularTextInputCell(_ tableView: UITableView, _ indexPath: IndexPath, _ chainproperty: ChainProperty) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateAssetCell", for: indexPath) as! CreateAssetCell
        let displayText = chainproperty.displayName ?? chainproperty.propertyName
        cell.lblFieldTitle.text =  displayText
        cell.tfFieldValue.placeholder = "Enter \(displayText)"
        cell.tfFieldValue.delegate = self
        cell.tfFieldValue.tag = indexPath.row
        
        if let value = chainproperty.propertyValue, value is NSNull == false {
            var text = ""
            if let array = value as? Array<Any> {
                for i in 0..<array.count {
                    let item = array[i]
                    text+="\(item)"
                    if i < array.count-1 {
                        text+=";"
                    }
                }
            }
            else {
                text = "\(value)"
            }
            cell.tfFieldValue.text = text
        }
        // Configure the cell...
        
        return cell
    }
    
    fileprivate func customInputCell(_ tableView: UITableView, _ indexPath: IndexPath, _ chainproperty: ChainProperty) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomActionCell", for: indexPath) as! CustomActionCell
        let displayText = chainproperty.displayName ?? chainproperty.propertyName
        cell.lblFieldTitle.text = displayText
        cell.btnCustomAction.setTitle("Select \(displayText)", for: .normal)
        cell.btnCustomAction.tag = indexPath.row
        cell.btnCustomAction.addTarget(self, action: #selector(customSelectAction(_:)), for: .touchUpInside)
        // Configure the cell...
        
        return cell
    }

    @objc func customSelectAction(_ sender: Any) {
        if let sender = sender as? UIButton {
            let chainproperty = model.chainProperies[sender.tag]
            chainproperty.customAction?(sender)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chainproperty = model.chainProperies[indexPath.row]
        if (chainproperty.requiresCustomInputAction == false)
        {
            return regularTextInputCell(tableView, indexPath, chainproperty)
        }
        else
        {
            return customInputCell(tableView, indexPath, chainproperty)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let cell = tableView.cellForRow(at: IndexPath(row: textField.tag, section: 0)) as? CreateAssetCell,
            let value = cell.tfFieldValue.text {
            let chainproperty = model.chainProperies[textField.tag]
            chainproperty.propertyValue = value
        }
    }
    
    func populate() {
        self.view.endEditing(true)
        let sv = BaseViewController.displaySpinner(onView: self.view)
        model.saveAsset(completionHandler:  { (asset, error) in
            BaseViewController.removeSpinner(spinner: sv)
            if let _ = asset {
                if let _  = self.navigationController {
                    self.navigationController?.popViewController(animated: true)
                    self.model.completionHandler?(asset,error)
                }
                else {
                    self.dismiss(animated: true, completion: {
                        self.model.completionHandler?(asset,error)
                    })
                }
            }
            else {
                self.showAlert("Error", "\((error as? Error)?.localizedDescription ?? "")")
            }
        })
        
        
    }
    
    public func showAlert(_ title: String!, _ alertMessage: String!, _ handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        guard   let _ = alertMessage,
            let _ = title else {
                return
        }
        
        let alert = UIAlertController(title: title!, message: alertMessage!, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "I've got it", style: .default, handler: handler))
        self.present(alert, animated: true)
        
    }

}
