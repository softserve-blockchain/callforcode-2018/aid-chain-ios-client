//
//  RoleSelectionController.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class RoleSelectionController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private static let roleSelectCellId = "RoleSelectionCellId"
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ChainAssets.allParticipants.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RoleSelectionController.roleSelectCellId, for: indexPath) as! RoleSelectionCell

        cell.btnSelectRole.setTitle(ChainAssets.allParticipants[indexPath.row].rawValue, for: .normal)
        cell.btnSelectRole.addTarget(self, action: #selector(assetSelected), for: .touchUpInside)
        cell.btnSelectRole.tag = indexPath.row
        return cell
    }
    
    @objc func assetSelected(sender: UIButton!) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.selectedRole = ChainAssets.allParticipants[sender.tag]
            print("Selected role: \(delegate.selectedRole!.rawValue)")
        }
        self.performSegue(withIdentifier: "loginSegue", sender: self)
    }

}
