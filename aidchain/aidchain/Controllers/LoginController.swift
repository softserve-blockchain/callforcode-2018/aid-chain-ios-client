//
//  LoginController.swift
//  Fraud-detection
//
//  Created by Doronin Denis on 5/24/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit
import SafariServices

class LoginController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var authSession: SFAuthenticationSession?
    public var model: LoginModel?
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfLogin: UITextField!
    @IBOutlet weak var lblSelectedRole: UILabel!
    
    private var picker: PickerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        model = LoginModel()
        self.navigationItem.hidesBackButton = true
        tfLogin.delegate = self
        tfPassword.delegate = self
        
        self.lblSelectedRole.text = "for \(model!.selectedRole().rawValue)"
        if model!.shouldUserSetupPassword() {
            self.performSegue(withIdentifier: "setPasswordSegue", sender: self)
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Change Role", style: .done, target: self, action: #selector(actionRole(_:)))
        
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "storedUsers"), for: .normal)
        let globalInset = tfLogin.frame.size.height * 0.15
        button.imageEdgeInsets = UIEdgeInsets(top: globalInset, left: globalInset, bottom: globalInset, right: globalInset)
        button.frame = CGRect(x: CGFloat(tfLogin.frame.size.width - tfLogin.frame.size.height), y: CGFloat(0), width: CGFloat(tfLogin.frame.size.height), height: CGFloat(tfLogin.frame.size.height))
        button.addTarget(self, action: #selector(self.selectStoredUser), for: .touchUpInside)
        tfLogin.rightView = button
        tfLogin.rightViewMode = .always
        
    }
    @objc func selectStoredUser(_ sender: Any) {
        
        self.picker = PickerController()
        if let picker = self.picker {
            picker.title = "Select user from list"
            let loginManager = LoginManager.fetchLoginManager()
            if let values = loginManager.storedUsers?.values {
                picker.presentPicker(presenter: self, data: Array(values), titlefetcher: { (user) -> String in
                    if let user = user as? User {
                        return "\(user.login ?? ""):\(user.assetTypeString ?? "")"
                    }
                    return ""
                }, completion: { selected in
                    if let user = selected as? User {
                        self.tfLogin.text =  user.login
                    }
                })
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let loginManager = LoginManager.fetchLoginManager()
        return loginManager.storedUsers?.values.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       let loginManager = LoginManager.fetchLoginManager()
        if let values = loginManager.storedUsers?.values {
           return Array(values)[row].login
        }
        return ""
    }

    
    @objc func actionRole(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.selectedRole = nil
            print("Change role action")
        }
    }

    @IBAction func changeEnpointAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Endpoint", message: "Please enter new URL", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Endpoint URL"
            textField.text = BlockchainConnector.endpointURLString()
        }
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0], let text = textField.text {
                _ = BlockchainConnector.saveEndpointURLString(endpointURLString:text )
            }

        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblSelectedRole.text = "for \(model!.selectedRole().rawValue)"
         self.tfLogin.text = model?.userLoginId() ?? ""
    }
    
    @IBAction func login(_ sender: Any) {
        let (res,errorMessage) = model!.validateInput(loginString: tfLogin.text, passString: tfPassword.text)
        
        
        switch res {
        case LoginError.FieldValidation:
            showAlert("Validation warning",errorMessage)
            return
        case LoginError.UserDoesNotExists:
            showAlert("Error",errorMessage)
            return
        case .PasswordMismatch:
            showAlert("Error", errorMessage)
            return
        case .None:
            print("Login succeed")
        }
        
        let sv = BaseViewController.displaySpinner(onView: self.view)
        
        
        model!.verifyUser(userId: tfLogin.text!) { (success) in
            BaseViewController.removeSpinner(spinner: sv)
            if success {
                self.tfPassword.text = nil
                self.performSegue(withIdentifier: "segueMainMenu", sender: self)
            } else {
                self.showAlert("Error", "Cannot login user")
            }
        }
    }
    
    @IBAction func createAssetAction(_ sender: Any) {
        self.performSegue(withIdentifier: "createAssetSegue", sender: self)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "createAssetSegue" {
            if let vc = segue.destination as? SaveAssetController {
                let vcmodel = SaveAssetModel(model!.selectedRole(), nil)
                vc.navigationItem.title = NSLocalizedString("Register ", comment: "") + vcmodel.modelType.rawValue.lowercased()
                vcmodel.completionHandler = {
                    (asset: Any?, error: Any?) -> Void in
                    if let user = asset as? ChainEntity {
                        
                        self.model!.saveUser(user: user, completion: { (success) in
                            if success {
                                self.performSegue(withIdentifier: "setPasswordSegue", sender: self)
                            } else {
                                self.showAlert("Error", "Cannot save the user")
                            }
                        })
                    }
                }
                vc.model = vcmodel
                
            }
        }
        
        if segue.identifier == "setPasswordSegue" {
            if let login = model!.userLoginId() {
                if let vc = segue.destination as? SetPasswordController {
                    vc.entityId = login
                }
            }
        }
        
        if segue.identifier == "segueMainMenu" {
            if let vc = segue.destination as? AvailableAssetsController {
                let vcmodel = AvailableAssetsModel(role: (model?.selectedRole())!)
                vc.assetsModel = vcmodel
            }
            
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if tfPassword == textField {
            login(textField)
        }
        return super.textFieldShouldReturn(textField)
    }

}
