//
//  PickerController.swift
//  aidchain
//
//  Created by Denys Doronin on 9/11/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class PickerController: NSObject, UIPickerViewDelegate, UIPickerViewDataSource{

    
    public  var title: String = "Select value from list"
    public  var successActionTitle: String = "OK"
    public  var cancelActionTitle: String = "Cancel"
    
    private var data:[Any]?
    private var titlefetcher: ((Any)->String)?
    
    public func presentPicker(presenter: UIViewController, data: [Any], titlefetcher: @escaping ((Any)->String), completion: ((Any)->Void)? = nil ){
        
        self.data = data
        self.titlefetcher = titlefetcher
        let alertView = UIAlertController(
            title: title,
            message: "\n\n\n\n\n\n\n\n\n",
            preferredStyle: .alert)
        
        let pickerView = UIPickerView(frame:
            CGRect(x: 0, y: 50, width: 260, height: 162))
        pickerView.dataSource = self
        pickerView.delegate = self
        
        // comment this line to use white color
        pickerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        alertView.view.addSubview(pickerView)
        
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: alertView.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: alertView.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: alertView.view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 100)
        let heightConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 162)
        alertView.view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        let saveAction = UIAlertAction(title: successActionTitle, style: .default, handler: { alert -> Void in
            if let selected = self.data?[pickerView.selectedRow(inComponent: 0)] {
                completion?(selected)
            }
        })
        let cancelAction = UIAlertAction(title: cancelActionTitle, style: .default, handler: { (action : UIAlertAction!) -> Void in })
        alertView.addAction(saveAction)
        alertView.addAction(cancelAction)
        
        presenter.present(alertView, animated: true) {
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.data?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        if let object = data?[row], let fetcher = titlefetcher {
            return fetcher(object)
        }
        
        return ""
    }
    
}
