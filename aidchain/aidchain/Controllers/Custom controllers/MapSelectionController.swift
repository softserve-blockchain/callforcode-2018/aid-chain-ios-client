//
//  MapSelectionController.swift
//  aidchain
//
//  Created by Denys Doronin on 9/5/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit
import GoogleMaps

class MapSelectionController: BaseViewController {

    @IBOutlet weak var mapView: GMSMapView!

    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var zoomLevel: Float = 15.0
    var usermarker: GMSMarker?
    var spinnerView: UIView?
    
    var completionHandler: ((Location)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()

        let camera = GMSCameraPosition.camera(withLatitude: 0.0,
                                              longitude: 0.0,
                                              zoom: zoomLevel)
        mapView.camera = camera
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        mapView.isHidden = true
   
        spinnerView = BaseViewController.displaySpinner(onView: self.view)
    
        self.navigationItem.title = "Select delivery point"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(placeSelected(_:)))
    }
    
    @objc func placeSelected(_ sender: Any) {
        
        guard let usermarker = usermarker else {
            showAlert("Warning", "Target place should be selected")
            return
        }
        
        let selectedLocation = Location(latitude: usermarker.position.latitude, longitude: usermarker.position.longitude)
        completionHandler?(selectedLocation)
    }

    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MapSelectionController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        usermarker = GMSMarker(position: coordinate)
        if let usermarker = usermarker {
            mapView.clear()
            usermarker.title = "Target delivery place"
            usermarker.isDraggable = true
            usermarker.icon = UIImage(named: "targetMapPlaceIcon")
            usermarker.map = mapView
        }
        
    }
}

extension MapSelectionController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    fileprivate func removeSpinner() {
        if let spinner = spinnerView {
            BaseViewController.removeSpinner(spinner: spinner)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        currentLocation = location
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if mapView.isHidden {
            removeSpinner()
            
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        removeSpinner()
        
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        removeSpinner()
        print("Error: \(error)")
    }
}
