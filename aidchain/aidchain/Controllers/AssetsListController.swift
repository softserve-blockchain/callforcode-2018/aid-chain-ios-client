//
//  DetailsController.swift
//  Fraud-detection
//
//  Created by Doronin Denis on 5/25/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit

class AssetsListController: UITableViewController, DetailsModelDelegate {
    
    
    @IBOutlet weak var filterButton: UIBarButtonItem!
    @IBOutlet weak var createAssetButton: UIBarButtonItem!
    private static let detailsCellId = "detailsCell"
    private static let claimAssetCellId = "ClaimAssetCell"
    private static let transactionCellId = "transactionCellId"
    
    private var spinnerView :UIView? = nil
    private var picker: PickerController?
    
    public var model: AssetListModel!
    
    func modelDidStartActivity() {
        tableView.refreshControl?.beginRefreshing()
    }
    
    func modelDidFinishActivity() {
        tableView.refreshControl?.endRefreshing()
    }
    
    func modelDidUpdate() {
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = typeToAsset(type: model.assetType)?.rawValue
        filterButton.isEnabled = model.isFilterAvailable()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.attributedTitle = NSAttributedString(string: NSLocalizedString("Fetching Data ...", comment: ""), attributes: nil)
        tableView.refreshControl?.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        AssetCellFactory.registerAssetCells(tableView: self.tableView)
        model.delegate = self
        model.fetchAssets()
    }
    
    @objc private func refreshData(_ sender: Any) {
        //self.tableView.refreshControl?.endRefreshing()
        model.fetchAssets()
    }
    
    @IBAction func selectFilter(_ sender: Any) {
        
        self.picker = PickerController()
        if let picker = self.picker {
            picker.title = NSLocalizedString("Select filter from list", comment: "")
            let values = [AssetFilterOptions.ALL, AssetFilterOptions.USER]
            
            picker.presentPicker(presenter: self, data: Array(values), titlefetcher: { (value) -> String in
                return (value as? AssetFilterOptions)?.rawValue ?? ""
            }, completion: { selected in
                if let option = selected as? AssetFilterOptions {
                    self.model.setFilterAndUpdate(filter: option)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.assets?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = model.assets?[indexPath.row]
        return AssetCellFactory.tableView(item: item, tableView: tableView, cellForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (model.allowedToUpdate)
        {
            
            if let itemToUpdate = model.assets?[indexPath.row] as? ChainEntity, let vc = self.storyboard?.instantiateViewController(withIdentifier: "SaveAssetController") as? SaveAssetController {
                prepareModel(vc: vc,itemToUpdate: itemToUpdate)
                vc.isUpdateScenario = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }

    @IBAction func createAssetAction(_ sender: Any) {
        self.performSegue(withIdentifier: "createAssetSegue", sender: self)
    }
    
    
    func prepareModel(vc: SaveAssetController, itemToUpdate: ChainEntity? = nil) {
        var vcmodel: SaveAssetModel?
        
        if let asset = typeToAsset(type: model.assetType) {
            vcmodel = SaveAssetModel(asset, itemToUpdate)
            let prefix = itemToUpdate != nil ? NSLocalizedString("Update ", comment: "") : NSLocalizedString("Create ", comment: "")
            vc.navigationItem.title = prefix + asset.rawValue.lowercased()
        }
        
        if let _ = vcmodel {
            vc.model = vcmodel
        }
        
        vcmodel?.completionHandler = {
            (asset: Any?, error: Any?) -> Void in
            if let _ = asset {
                self.model.fetchAssets()
            }
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "createAssetSegue" {
            if let vc = segue.destination as? SaveAssetController, let _ = model {
                prepareModel(vc: vc)
            }
        }
    }

}
