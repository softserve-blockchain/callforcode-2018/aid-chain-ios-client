//
//  CreateAssetCell.swift
//  Fraud-detection
//
//  Created by Denys Doronin on 5/30/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit

class CreateAssetCell: UITableViewCell {

    @IBOutlet weak var tfFieldValue: UITextField!
    @IBOutlet weak var lblFieldTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
