//
//  CustomActionCell.swift
//  aidchain
//
//  Created by Denys Doronin on 9/6/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class CustomActionCell: UITableViewCell {

    @IBOutlet weak var lblFieldTitle: UILabel!
    @IBOutlet weak var btnCustomAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
