//
//  TransactionCell.swift
//  Fraud-detection
//
//  Created by Denys Doronin on 6/14/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet weak var lblTitleTransaction: UILabel!
    @IBOutlet weak var lblTitleBlock: UILabel!
    @IBOutlet weak var lblClaim: UILabel!
    @IBOutlet weak var lblValueTransaction: UILabel!
    @IBOutlet weak var lblValueBlock: UILabel!
    @IBOutlet weak var lblValueDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
