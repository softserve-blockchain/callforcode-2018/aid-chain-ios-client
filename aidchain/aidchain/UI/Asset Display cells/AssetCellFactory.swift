//
//  AssetCellFactory.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class AssetCellFactory {
    
    static let defaultCellId = "detailsCell"

    public class func registerAssetCells(tableView: UITableView) {
        tableView.register(UINib(nibName: "RequestDisplayCell", bundle: nil), forCellReuseIdentifier: Request.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "CommodityCell", bundle: nil), forCellReuseIdentifier: Commodity.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "BucketCell", bundle: nil), forCellReuseIdentifier: Bucket.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "OpenDonateCell", bundle: nil), forCellReuseIdentifier: OpenDonateTransaction.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "ApproveRequestCell", bundle: nil), forCellReuseIdentifier: ApproveRequestTransaction.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "PurchaseBucketCell", bundle: nil), forCellReuseIdentifier: PurchaseBucketTransaction.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "InventoryRegCell", bundle: nil), forCellReuseIdentifier: InventoryBucketRegistrationTransaction.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "StartDeliveryCell", bundle: nil), forCellReuseIdentifier: StartBucketDeliveryTransaction.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "FinishDeliveryCell", bundle: nil), forCellReuseIdentifier: FinishBucketDeliveryTransaction.entityClassString() + "CellId")
        tableView.register(UINib(nibName: "DonateCell", bundle: nil), forCellReuseIdentifier: DonateTransaction.entityClassString() + "CellId")
        
        
    }

    public class func tableView(item:Any?, tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: defaultCellId, for: indexPath)
        
        if let request = item as? Request {
            let acell = tableView.dequeueReusableCell(withIdentifier: Request.entityClassString() + "CellId", for: indexPath) as! RequestDisplayCell
            acell.populateCell(entity: request)
            cell = acell
        }
        
        if let commodity = item as? Commodity {
            let acell = tableView.dequeueReusableCell(withIdentifier: Commodity.entityClassString() + "CellId", for: indexPath) as! CommodityCell
            acell.populateCell(entity: commodity)
            cell = acell
        }
        
        if let bucket = item as? Bucket {
            let acell = tableView.dequeueReusableCell(withIdentifier: Bucket.entityClassString() + "CellId", for: indexPath) as! BucketCell
            acell.populateCell(entity: bucket)
            cell = acell
        }
        
        if let tx = item as? OpenDonateTransaction {
            let acell = tableView.dequeueReusableCell(withIdentifier: OpenDonateTransaction.entityClassString() + "CellId", for: indexPath) as! OpenDonateCell
            acell.populateCell(entity: tx)
            cell = acell
        }
        
        if let tx = item as? ApproveRequestTransaction {
            let acell = tableView.dequeueReusableCell(withIdentifier: ApproveRequestTransaction.entityClassString() + "CellId", for: indexPath) as! ApproveRequestCell
            acell.populateCell(entity: tx)
            cell = acell
        }
        
        if let tx = item as? DonateTransaction {
            let acell = tableView.dequeueReusableCell(withIdentifier: DonateTransaction.entityClassString() + "CellId", for: indexPath) as! DonateCell
            acell.populateCell(entity: tx)
            cell = acell
        }
        
        if let tx = item as? PurchaseBucketTransaction {
            let acell = tableView.dequeueReusableCell(withIdentifier: PurchaseBucketTransaction.entityClassString() + "CellId", for: indexPath) as! PurchaseBucketCell
            acell.populateCell(entity: tx)
            cell = acell
        }
        
        if let tx = item as? InventoryBucketRegistrationTransaction {
            let acell = tableView.dequeueReusableCell(withIdentifier: InventoryBucketRegistrationTransaction.entityClassString() + "CellId", for: indexPath) as! InventoryRegCell
            acell.populateCell(entity: tx)
            cell = acell
        }
        
        if let tx = item as? StartBucketDeliveryTransaction {
            let acell = tableView.dequeueReusableCell(withIdentifier: StartBucketDeliveryTransaction.entityClassString() + "CellId", for: indexPath) as! StartDeliveryCell
            acell.populateCell(entity: tx)
            cell = acell
        }
        
        if let tx = item as? FinishBucketDeliveryTransaction {
            let acell = tableView.dequeueReusableCell(withIdentifier: FinishBucketDeliveryTransaction.entityClassString() + "CellId", for: indexPath) as! FinishDeliveryCell
            acell.populateCell(entity: tx)
            cell = acell
        }
        // Configure the cell...
        
        return cell
    }
}
