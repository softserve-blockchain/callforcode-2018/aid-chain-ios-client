//
//  PurchaseBucketCell.swift
//  aidchain
//
//  Created by Denys Doronin on 9/13/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class PurchaseBucketCell: UITableViewCell {

    @IBOutlet weak var lblidentifierTitle: UILabel!
    
    @IBOutlet weak var lblidentifier: UILabel!
    @IBOutlet weak var lbltimestamp: UILabel!
    @IBOutlet weak var lblasset: UILabel!
    @IBOutlet weak var lblparticipant: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populateCell(entity: ChainEntity) {
        lblidentifierTitle.text = NSLocalizedString("Purchase bucket transaction identifier:", comment: "")
        if let tx = entity as? PurchaseBucketTransaction {
            lblidentifier.text = "\(tx.entityId ?? "Unknown")"
            
            lblasset.text = NSLocalizedString("Bucket: ", comment: "") + "\(tx.asset?.entityId ?? "Unknown")"
            lblparticipant.text = NSLocalizedString("Purchased by: ", comment: "") + "\(tx.processor?.entityId ?? "Unknown")"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm, dd MMM yyyy"
            
            if let creationTime = tx.timestamp {
                lbltimestamp.text = NSLocalizedString("Created: ", comment: "") + formatter.string(from: creationTime)
            } else {
                lbltimestamp.text = NSLocalizedString("Creation date is unknown", comment: "")
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
