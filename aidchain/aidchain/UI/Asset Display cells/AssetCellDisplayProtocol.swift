//
//  AssetCellDisplayProtocol.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import Foundation

protocol AssetCellDisplayProtocol {
    func populateCell(entity: ChainEntity)
}
