//
//  DonateCell.swift
//  aidchain
//
//  Created by Denys Doronin on 9/13/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class DonateCell: UITableViewCell {

    @IBOutlet weak var lblidentifierTitle: UILabel!
    
    @IBOutlet weak var lblidentifier: UILabel!
    @IBOutlet weak var lbltimestamp: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lbldonator: UILabel!
    @IBOutlet weak var lblfund: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populateCell(entity: ChainEntity) {
        lblidentifierTitle.text = NSLocalizedString("Donate transaction:", comment: "")
        if let tx = entity as? DonateTransaction {
            lblidentifier.text = "\(tx.entityId ?? "Unknown")"
            
            lblamount.text = NSLocalizedString("Amount: ", comment: "") + "\(String(format: "%.2f", tx.amount))$"
            
            lbldonator.text = NSLocalizedString("From: ", comment: "") + "\(tx.donor?.entityId ?? "Unknown")"
            lblfund.text = NSLocalizedString("To fund: ", comment: "") + "\(tx.destination?.entityId ?? "Unknown")"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm, dd MMM yyyy"
            
            if let creationTime = tx.timestamp {
                lbltimestamp.text = NSLocalizedString("Created: ", comment: "") + formatter.string(from: creationTime)
            } else {
                lbltimestamp.text = NSLocalizedString("Creation date is unknown", comment: "")
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
