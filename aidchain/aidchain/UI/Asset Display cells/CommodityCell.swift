//
//  CommodityCell.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class CommodityCell: UITableViewCell, AssetCellDisplayProtocol {

    
    @IBOutlet weak var lblcommodityDescTitle: UILabel!
    
    @IBOutlet weak var lblcommodityDesc: UILabel!
    @IBOutlet weak var lblcommodityType: UILabel!
    @IBOutlet weak var lblcommodityIdentifier: UILabel!
    @IBOutlet weak var lblcommodityPrice: UILabel!
    @IBOutlet weak var lblcommodityWeight: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func populateCell(entity: ChainEntity) {
        lblcommodityDescTitle.text = NSLocalizedString("Commodity description:", comment: "")
        if let commodity = entity as? Commodity {
            lblcommodityIdentifier.text = NSLocalizedString("Identifier: ", comment: "") + "\(commodity.entityId ?? "Unknown")"
            lblcommodityDesc.text = commodity.commodityDescription
            lblcommodityType.text = NSLocalizedString("Type: ", comment: "") + "\(commodity.commodityType ?? "Unknown")"
            lblcommodityPrice.text = NSLocalizedString("Price: ", comment: "") + "\(commodity.purchasePrice)"
            lblcommodityWeight.text = NSLocalizedString("Weight (kg): ", comment: "") + "\(commodity.weightKg)"
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
