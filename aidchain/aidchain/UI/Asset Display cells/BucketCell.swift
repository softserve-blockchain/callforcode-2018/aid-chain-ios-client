//
//  BucketCell.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class BucketCell: UITableViewCell, AssetCellDisplayProtocol{
    
    @IBOutlet weak var lblcommoditiesTitle: UILabel!
    
    @IBOutlet weak var lblcommodities: UILabel!
    @IBOutlet weak var lblbucketstatus: UILabel!
    @IBOutlet weak var lblrequest: UILabel!
    @IBOutlet weak var lblbucketidentifier: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populateCell(entity: ChainEntity) {
        lblcommoditiesTitle.text = NSLocalizedString("Commodities:", comment: "")
        if let bucket = entity as? Bucket {
            
            if let  commodities = bucket.commodities {
                var res = "\n"
                for commodity in commodities {
                    res += NSLocalizedString("Commodity: ", comment: "") + "\(commodity.entityId ?? "Unknown")"
                    res += "\n"
                }
                lblcommodities.text = res
            }
            
            lblrequest.text = NSLocalizedString("Request: ", comment: "") + "\(bucket.aidRequest?.entityId ?? "Unknown")"
            lblbucketidentifier.text = NSLocalizedString("Identifier: ", comment: "") + "\(bucket.entityId ?? "Unknown")"
            lblbucketstatus.text = NSLocalizedString("Status: ", comment: "") + "\(bucket.bucketStatus ?? "Unknown")"
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
