//
//  OpenDonateCell.swift
//  aidchain
//
//  Created by Denys Doronin on 9/13/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class OpenDonateCell: UITableViewCell {

    
    @IBOutlet weak var lblidentifierTitle: UILabel!
    
    @IBOutlet weak var lblidentifier: UILabel!
    @IBOutlet weak var lbltimestamp: UILabel!
    @IBOutlet weak var lblasset: UILabel!
    @IBOutlet weak var lblparticipant: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populateCell(entity: ChainEntity) {
        lblidentifierTitle.text = NSLocalizedString("Open donate transaction identifier:", comment: "")
        if let tx = entity as? OpenDonateTransaction {
            lblidentifier.text = "\(tx.entityId ?? "Unknown")"
            
            lblasset.text = NSLocalizedString("Request: ", comment: "") + "\(tx.asset?.entityId ?? "Unknown")"
            lblparticipant.text = NSLocalizedString("For seeker: ", comment: "") + "\(tx.initiator?.entityId ?? "Unknown")"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm, dd MMM yyyy"
            
            if let creationTime = tx.timestamp {
                lbltimestamp.text = NSLocalizedString("Created: ", comment: "") + formatter.string(from: creationTime)
            } else {
                lbltimestamp.text = NSLocalizedString("Creation date is unknown", comment: "")
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
