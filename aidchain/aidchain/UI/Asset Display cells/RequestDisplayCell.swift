//
//  RequestDisplayCell.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class RequestDisplayCell: UITableViewCell,AssetCellDisplayProtocol {
    
    @IBOutlet weak var lblaiddescriptionTitle: UILabel!
    
    @IBOutlet weak var lblidentifier: UILabel!
    @IBOutlet weak var lblaiddescription: UILabel!
    @IBOutlet weak var lblrequestStatus: UILabel!
    @IBOutlet weak var lblseeker: UILabel!
    @IBOutlet weak var lblapprover: UILabel!
    @IBOutlet weak var lbltargetLocation: UILabel!
    @IBOutlet weak var lblcreationTime: UILabel!
    @IBOutlet weak var lbldeliveryTime: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populateCell(entity: ChainEntity) {
        lblaiddescriptionTitle.text = NSLocalizedString("Request description:", comment: "")
        if let request = entity as? Request {
            
            lblidentifier.text = NSLocalizedString("Identifier: ", comment: "") + "\(request.entityId ?? "Unknown")"
            
            lblaiddescription.text = request.aiddescription
            lblrequestStatus.text = NSLocalizedString("Status: ", comment: "") + "\(request.requestStatus ?? "Unknown")"
            
            lblseeker.text = NSLocalizedString("Seeker: ", comment: "") + "\(request.seeker?.entityId ?? "Unknown")"
            lblapprover.text = NSLocalizedString("Approver: ", comment: "") + "\(request.approver?.entityId ?? "Unknown")"
            
            lbltargetLocation.text = NSLocalizedString("Target location: ", comment: "") + "\(request.targetLocation?.latitude ?? 0.0);\(request.targetLocation?.longitude ?? 0.0)"
            let formatter = ChainEntity.dateFormatter()
            
            if let creationTime = request.creationTime {
                lblcreationTime.text = NSLocalizedString("Created: ", comment: "") + formatter.string(from: creationTime)
            } else {
                lblcreationTime.text = NSLocalizedString("Creation date is unknown", comment: "")
            }
            
            if let deliveryTime = request.deliveryTime {
                lbldeliveryTime.text = NSLocalizedString("Delivered: ", comment: "") + formatter.string(from: deliveryTime)
            } else {
                lbldeliveryTime.text = NSLocalizedString("Request is not delivered yet", comment: "")
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
