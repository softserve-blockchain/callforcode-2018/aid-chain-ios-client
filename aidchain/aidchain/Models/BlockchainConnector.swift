//
//  BlockchainConnector.swift
//  Fraud-detection
//
//  Created by Doronin Denis on 5/24/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit

class BlockchainConnector: NSObject {

    static let endpointURLKey = "endpointURLKey"
    static let defaultEndpointURLString = "https://aid-chain-rest.eu-gb.mybluemix.net/api"

    private var urlSessionDataTask: URLSessionDataTask!

    public class func endpointURLString() -> String {
        if let endpointURLString = Keychain.load(key:endpointURLKey).flatMap({ String(bytes:$0, encoding: .utf8) }) {
            return endpointURLString
        }
        return defaultEndpointURLString
    }
    
    public class func saveEndpointURLString(endpointURLString: String)->Bool {
        
        let status = Keychain.save(key: endpointURLKey, data: endpointURLString.data(using: .utf8)!)
        
        if status == 0 {
            return true
        }
        return false
    }
    
    private func handleResponse(_ response: URLResponse?, _ error: Error?, _ data: Data?, completionHandler: ((Data?, Any?) -> Void)?) {
        if let response = response as? HTTPURLResponse {
            if let error = error {
                completionHandler?(nil,error)
            } else if let data = data {
                if response.statusCode == 200 {
                     completionHandler?(data,error)
                } else {
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let errorDic = json?["error"] as? [String:Any]
                        let message  = errorDic?["message"] as? String
                        let error    = NSError(domain:"aid.aidchain",
                                               code:response.statusCode,
                                               userInfo:[NSLocalizedDescriptionKey:message ?? ""])
                        completionHandler?(nil,error)
                    }
                }
            } else {
                let error = NSError(domain:"aid.aidchain",
                                    code:-1,
                                    userInfo:[NSLocalizedDescriptionKey:"Unknown error"])
                completionHandler?(nil,error)
            }
        } else {
            completionHandler?(nil,error)
        }
    }
    
    private func processSingleEntityRequest(_ assetType: ChainEntity.Type, _ request: URLRequest, _ completionHandler: ((ChainEntity?, Any?) -> Void)?) {
        URLSession.log(request: request)
        
        urlSessionDataTask = URLSession.shared.dataTask(with: request) { data, response, error in
            
            URLSession.log(data: data, response: response as? HTTPURLResponse, error: error)
            defer { self.urlSessionDataTask = nil }
            self.handleResponse(response, error, data,completionHandler: { data, error in
                var asset: ChainEntity? = nil
                if let data = data {
                    asset = assetType.decodeSelf(data: data) as? ChainEntity
                }
                DispatchQueue.main.async {
                    completionHandler?(asset,error)
                }
            })
        }
        urlSessionDataTask!.resume()
    }
    
    func updateAsset(asset: ChainEntity, completionHandler: ((_ createdAsset: ChainEntity?,_ error:Any?) -> Void)? = nil) -> Void {
        urlSessionDataTask?.cancel()
        let createAssetUrlString = BlockchainConnector.endpointURLString() + "/" + type(of: asset).entityClassString()
        guard let request = asset.updateEntityRequest(createAssetUrlString ) as URLRequest? else { return }
        
        processSingleEntityRequest(type(of: asset), request, completionHandler)
    }
    
    func createAsset(newAsset: ChainEntity, completionHandler: ((_ createdAsset: ChainEntity?,_ error:Any?) -> Void)? = nil) -> Void {
        urlSessionDataTask?.cancel()
        
        let createAssetUrlString = BlockchainConnector.endpointURLString() + "/" + type(of: newAsset).entityClassString()
        guard let request = newAsset.createEntityRequest(createAssetUrlString ) as URLRequest? else { return }
        
        processSingleEntityRequest(type(of: newAsset), request, completionHandler)
    }
    
    func getAssets(assetType: ChainEntity.Type,  filterDictionary: [String:Codable]?, completionHandler: ((_ assets: [ChainEntity]?, _ error:Any?) -> Void)? = nil) -> Void {
        urlSessionDataTask?.cancel()
        
        let getAssetURLString  = BlockchainConnector.endpointURLString() + "/" + assetType.entityClassString()
        
        do {
            
            let urlComps       = NSURLComponents(string: getAssetURLString)!
            if let _ = filterDictionary {
                let jsonData       = try JSONSerialization.data(withJSONObject: filterDictionary!, options: .prettyPrinted)
                let queryItems     = [URLQueryItem(name: "filter", value: String(data: jsonData, encoding: String.Encoding.utf8))]
                urlComps.queryItems = queryItems
            }
            let userURL        = urlComps.url!
            var request        = URLRequest(url: userURL)
            request.httpMethod = "GET"
            
            URLSession.log(request: request)
            
            urlSessionDataTask = URLSession.shared.dataTask(with: request) { data, response, error in
                
                URLSession.log(data: data, response: response as? HTTPURLResponse, error: error)
                
                defer { self.urlSessionDataTask = nil }
                self.handleResponse(response, error, data,completionHandler: { data, error in
                    var assets: [ChainEntity]? = nil
                    if let data = data {
                        assets = assetType.decodeArray(data: data) as? [ChainEntity]
                    }
                    DispatchQueue.main.async {
                        completionHandler?(assets,error)
                    }
                })
            }
            urlSessionDataTask!.resume()
            
        }
        catch {
            
        }
    }
}
