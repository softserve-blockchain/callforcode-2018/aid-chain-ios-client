//
//  RuntimeHooks.swift
//  Fraud-detection
//
//  Created by Denys Doronin on 5/30/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import Foundation
extension NSObject {
    func propertyNames() -> [String] {
        return Mirror(reflecting: self).children.compactMap { $0.label }
    }
}
