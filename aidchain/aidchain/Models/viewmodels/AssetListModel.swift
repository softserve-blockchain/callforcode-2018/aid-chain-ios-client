//
//  AssetListModel.swift
//  aidchain
//
//  Created by Denys Doronin on 8/27/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

public enum AssetFilterOptions: String {
    case ALL = "All assets"
    case USER = "User's assets"
}

class AssetListModel {

    let connector = BlockchainConnector()
    
    var completionHandler: ((Any?, Any?)->Void)?
    var allowedToUpdate: Bool = false
    var assets: [Any]?
    let role: ChainAssets
    let assetType: ChainEntity.Type
    private var entityDecorator:ChainDecorator!
    private var assetFilter: AssetFilterOptions = .ALL
    private var assetFilterDictionary: [String:Codable]? = nil
    weak var delegate: DetailsModelDelegate?
    
    init(role: ChainAssets, assetType:ChainEntity.Type) {
       self.role = role
       self.assetType = assetType
    }
    
    public func isFilterAvailable()->Bool {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let _ = appDelegate.currentUser?.userFilter(assettype: assetType) {
            return true
        }
        return false
    }
    
    func setFilterAndUpdate(filter:AssetFilterOptions) {
        assetFilter = filter
        
        switch assetFilter {
        case .ALL:
            assetFilterDictionary = nil
            break
        case .USER:
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            assetFilterDictionary = appDelegate?.currentUser?.userFilter(assettype: assetType)
            break
        }
        fetchAssets()
    }
    
    func sortAssets(assets: [Any]?) {
        
        let completionHandler: (_ assets: [Any]?) -> Void = { (assets) in
            self.assets = assets
            self.delegate?.modelDidFinishActivity()
            self.delegate?.modelDidUpdate()
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            if let  txs = assets as? [ChainTransaction] {
                
                let sorted = txs.sorted { $0.timestamp!.compare($1.timestamp!) == .orderedDescending }
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    completionHandler(sorted)
                }
            }
            else{
                DispatchQueue.main.async {
                    completionHandler(assets)
                }
            }
        }
    }
    
    func fetchAssets() {
        delegate?.modelDidStartActivity()
        
        let completionHandler: (_ assets: [Any]?, _ error:Any?) -> Void = { (assets, error) in
            self.sortAssets(assets: assets)
        }
        
        connector.getAssets(assetType: assetType, filterDictionary: assetFilterDictionary, completionHandler: completionHandler)
    }
}
