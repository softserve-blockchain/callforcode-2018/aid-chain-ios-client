//
//  LoginModel.swift
//  aidchain
//
//  Created by Denys Doronin on 8/22/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

public enum LoginError {
    case None
    case FieldValidation
    case UserDoesNotExists
    case PasswordMismatch
}

class LoginModel: NSObject {

    let connector = BlockchainConnector()
    var completionHandler: ((Any?, Any?)->Void)?
    
    private func userPassword()->String? {
        
        let loginManager = LoginManager.fetchLoginManager()
        
        if let login = userLoginId(), let pass = loginManager.storedUsers?[login]?.password {
            return pass
        }
        return nil
    }
    
    public func selectedRole()->ChainAssets {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
           return delegate.selectedRole!
        }
        //Doro:concrete dependency should be avoided
        return ChainAssets.AidSeeker
    }
    
    public func userLoginId() -> String? {
        
        let loginManager = LoginManager.fetchLoginManager()
        
        if let login = loginManager.currentLoginId {
            return login
        }
        return nil
    }
    
    public func doesUserExist() -> Bool {
        
        if userLoginId() != nil {
            return true
        }
        
        return false
    }
    
    public func isPasswordSet() -> Bool {
        if doesUserExist() {
            if let pass = userPassword() {
                if !pass.isEmpty {
                    return true
                }
            }
        }
        return false
    }
    
    public func shouldUserSetupPassword() -> Bool {
        if doesUserExist() && isPasswordSet() == false {
            return true
        }
        return false
    }
    
    public func validateInput(loginString: String?, passString: String?) -> (LoginError, String?) {
        guard let login = loginString, !login.isEmpty,
            let pass = passString, !pass.isEmpty
            else { return (.FieldValidation, "Some fields are empty")}
        
        let loginManager = LoginManager.fetchLoginManager()
        
        guard let _ = LoginManager.userAssetType(by: login)  else {
            return (.UserDoesNotExists,"User was not created locally")
        }
        
        guard let storedPass = loginManager.storedUsers?[login]?.password, !storedPass.isEmpty && storedPass == pass  else {
            return (.PasswordMismatch,"Password doesn't match")
        }
        
        return (.None,nil)
    }
    
    public func verifyUser(userId: String, completion: ((Bool)->Void)?) -> Void {
        let filter :[String:Codable] = ["where":["entityId":userId]]
        let completion: (([ChainEntity]?, Any?) -> Void) = { (users, error) in
            if users?.count ?? 0 > 1 {
                fatalError("more than one user exist for same login. Blockchain is broken")
            }
            if let user = users?.first {
                
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.currentUser = user
                
                let loginManager =  LoginManager.fetchLoginManager()
                loginManager.currentLoginId = user.entityId
                _ = LoginManager.saveLoginManager(loginManager: loginManager)
                
                if typeToAsset(type: type(of:user)) != self.selectedRole() {
                    let delegate = UIApplication.shared.delegate as? AppDelegate
                    delegate?.selectedRole = typeToAsset(type: type(of:user))
                }
                
                completion?(true)
                return
            }
            completion?(false)
        }
        
        //let targetType = assetToType(asset: selectedRole())
        if let asset = LoginManager.userAssetType(by: userId) {
            let targetType = assetToType(asset:asset)
            connector.getAssets(assetType: targetType, filterDictionary: filter, completionHandler: completion)
        }
        else
        {
            fatalError("user was found on validation but dissappered on validation. Login flow is broken")
        }
        
    }
    
    public func saveUser(user:ChainEntity,completion: ((Bool)->Void)?) -> Void {
        if let entityId = user.entityId {
            
            let loginManager =  LoginManager.fetchLoginManager()
            
            let loginUser = User()
            loginUser.login = entityId
            loginUser.assetTypeString = typeToAsset(type: type(of: user))?.rawValue
            loginUser.password = nil
            
            if let _ = loginManager.storedUsers {
                
            } else
            {
                loginManager.storedUsers = [String:User]()
            }
            loginManager.currentLoginId = entityId
            loginManager.storedUsers?[entityId] = loginUser
            
            if LoginManager.saveLoginManager(loginManager: loginManager) {
                completion?(true)
                return
            }
//            completion?(success)
//            let status = Keychain.save(key: currentUserId, data: login.data(using: .utf8)!)
//            if status == 0 {
//                _ = Keychain.save(key: currentUserPassword, data:"".data(using: .utf8)!)
//                completion?(true)
//                return
//            }
        }
        completion?(false)
    }
    

}
