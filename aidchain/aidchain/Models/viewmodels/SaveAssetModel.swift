//
//  DetailsModel.swift
//  Fraud-detection
//
//  Created by Denys Doronin on 5/29/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import UIKit

enum DetailModelType {
    case policy
    case claim
    case transaction
}

protocol DetailsModelDelegate: class{
    func modelDidStartActivity()
    func modelDidFinishActivity()
    func modelDidUpdate()
}

class SaveAssetModel: NSObject {

    let connector = BlockchainConnector()
    
    var completionHandler: ((Any?, Any?)->Void)?
    
    var assets: [Any]?
    var modelType: ChainAssets!
    private var entityDecorator:ChainDecorator!
    private let updateScenario: Bool
    weak var delegate: DetailsModelDelegate?
    
    
    init(_ modelType: ChainAssets, _ asset: ChainEntity?) {
        self.modelType      = modelType
        self.updateScenario = asset != nil
        
        let targetAsset = asset ?? assetToType(asset: modelType).init()
        
        entityDecorator = ChainDecoratorFabric.createDecorator(entity: targetAsset, excludedKeys: targetAsset.excludedProperties())
        
    }
    
    public func currentUser()->ChainEntity {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            return delegate.currentUser!
        }
        return ChainEntity()
    }
    
    lazy var chainProperies:[ChainProperty] = {
        return entityDecorator.getChainProperties()
    }()

    func saveAsset(completionHandler:  ((_ createdAsset: Any?,_ error:Any?) -> Void)? = nil) {
        
        guard let modelType = modelType else {
            return
        }
        
        do {
            
            var values = [String:Any?]()
            
            for chainProperty in chainProperies {
                values[chainProperty.propertyName] = chainProperty.propertyValue
            }

           
            
            let connector = BlockchainConnector()
            
            
            
            if updateScenario == false {
                
                let jsonData = try JSONSerialization.data(withJSONObject: values, options: .prettyPrinted)
                let assetType = assetToType(asset: modelType)
                guard let asset = assetType.decodeSelf(data:jsonData) as? ChainEntity else {return}
                asset.setupEntity()
                
                //Doro:concrete dependency should be avoided
                if let request = asset as? Request {
                    request.seeker = currentUser() as? Seeker
                }
                
                connector.createAsset(newAsset: asset) { (createdAsset, error) in
                    completionHandler?(createdAsset,error)
                }
            }
            else {
                guard let asset = entityDecorator.updateEntity(dictionary: values) else
                {
                    completionHandler?(nil,nil)
                    return
                }
                connector.updateAsset(asset: asset) { (createdAsset, error) in
                    completionHandler?(createdAsset,error)
                }
            }
            
        }
        catch {
            print("Unexpected error: \(error).")
            completionHandler?(nil,error)
            print("gg")
        }
    }
}
