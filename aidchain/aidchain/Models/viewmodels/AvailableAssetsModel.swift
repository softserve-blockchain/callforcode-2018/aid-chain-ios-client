//
//  AvailableAssetsModel.swift
//  aidchain
//
//  Created by Denys Doronin on 8/27/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class AvailableAssetsModel {

    let targetRole: ChainAssets
    private let rules: [ChainEntityRule]?
    init(role: ChainAssets) {
        targetRole = role
        rules = ParticipantRulesFabric.participantRules(role: targetRole)
    }
    
    public lazy var assetsToView:[ChainEntity.Type]? = { return RulesProcessor.allowedAssetTypes(for: rules!, and: .READ )}()
    
    public func isAllowedToCreate(assetType:ChainEntity.Type) -> Bool {
        
        if let allowedTransaction = transactions?.contains(where: {$0 == assetType}) {
            if allowedTransaction == true {
                return true
            }
        }
        
        guard let result = (RulesProcessor.allowedAssetTypes(for: rules!, and: .CREATE )?.contains(where: {$0 == assetType})) else {return false}
        return result
    }
    
    public func isAllowedToUpdate(assetType:ChainEntity.Type) -> Bool {
        
        guard let result = (RulesProcessor.allowedAssetTypes(for: rules!, and: .UPDATE )?.contains(where: {$0 == assetType})) else {return false}
        return result
    }
    
    public lazy var transactions:[ChainEntity.Type]? = { return TransactionProcessor.allowedTransactionsTypes(for: targetRole)}()
    
    
}
