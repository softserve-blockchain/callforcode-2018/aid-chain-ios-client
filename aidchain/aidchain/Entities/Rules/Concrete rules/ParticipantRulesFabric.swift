//
//  ParticipantRulesFabric.swift
//  aidchain
//
//  Created by Denys Doronin on 8/27/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ParticipantRulesFabric {

    public class func participantRules(role:ChainAssets) -> [ChainEntityRule]? {

        // new case should be added in case when new role is added to the system
        switch role {
        case .AidDonor:
            return rulesForAidDonor()
        case .AidSeeker:
            return rulesForAidSeeker()
        case .AidFund:
            return rulesForAidFund()
        case .AidDrone:
            return rulesForAidDrone()
        case .AidInventory:
            return rulesForAidInventory()
        case .AidExchangeProcessor:
            return rulesForAidExchangeProcessor()
        default:
            return nil
        }
    }

    private class func rulesForAidDonor() -> [ChainEntityRule]? {
        
        var rule:ChainEntityRule
        var rules = [ChainEntityRule]()
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Donor can view requests"
        rule.participant     = Donor.self
        rule.operation       = [.READ]
        rule.resource        = Request.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Donor can view commodities"
        rule.participant     = Donor.self
        rule.operation       = [.READ]
        rule.resource        = Commodity.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        return rules
    }

    private class func rulesForAidSeeker() -> [ChainEntityRule]? {
        
        var rule:ChainEntityRule
        var rules = [ChainEntityRule]()
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Seeker has full acess to requests"
        rule.participant     = Seeker.self
        rule.operation       = [.CREATE, .READ, .UPDATE]
        rule.resource        = Request.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Seeker can view commodities"
        rule.participant     = Seeker.self
        rule.operation       = [.READ]
        rule.resource        = Commodity.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        return rules
    }
    
    private class func rulesForAidFund() -> [ChainEntityRule]? {
        
        var rule:ChainEntityRule
        var rules = [ChainEntityRule]()
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Fund can view & update requests"
        rule.participant     = Fund.self
        rule.operation       = [.READ, .UPDATE]
        rule.resource        = Request.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Fund can view commodities"
        rule.participant     = Fund.self
        rule.operation       = [.READ]
        rule.resource        = Commodity.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        return rules
    }
    
    private class func rulesForAidDrone() -> [ChainEntityRule]? {
        
        var rule:ChainEntityRule
        var rules = [ChainEntityRule]()
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Drone can view & update requests"
        rule.participant     = Drone.self
        rule.operation       = [.READ, .UPDATE]
        rule.resource        = Request.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        return rules
    }
    
    private class func rulesForAidInventory() -> [ChainEntityRule]? {
        
        var rule:ChainEntityRule
        var rules = [ChainEntityRule]()
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Inventory can view & update requests"
        rule.participant     = Inventory.self
        rule.operation       = [.READ, .UPDATE]
        rule.resource        = Request.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Inventory has full access to buckets"
        rule.participant     = Inventory.self
        rule.operation       = [.CREATE, .READ, .UPDATE]
        rule.resource        = Bucket.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Inventory has full access to commodities"
        rule.participant     = Fund.self
        rule.operation       = [.CREATE, .READ, .UPDATE]
        rule.resource        = Commodity.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        return rules
    }
    
    private class func rulesForAidExchangeProcessor() -> [ChainEntityRule]? {
        
        var rule:ChainEntityRule
        var rules = [ChainEntityRule]()
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Inventory can view & update requests"
        rule.participant     = ExchangeProcessor.self
        rule.operation       = [.READ, .UPDATE]
        rule.resource        = Request.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        rule                 = ChainEntityRule()
        rule.ruleDescription = "Inventory can view commodities"
        rule.participant     = ExchangeProcessor.self
        rule.operation       = [.READ]
        rule.resource        = Commodity.self
        rule.action          = .ALLOW
        
        rules.append(rule)
        
        return rules
    }
}
