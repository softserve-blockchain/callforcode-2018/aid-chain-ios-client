//
//  EntityRule.swift
//  aidchain
//
//  Created by Denys Doronin on 8/23/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit
struct ChainEntityOperations:OptionSet {
    
    public let rawValue: Int
    
    static let CREATE = ChainEntityOperations( rawValue: 1<<1)
    static let READ = ChainEntityOperations( rawValue: 1<<2)
    static let UPDATE = ChainEntityOperations( rawValue: 1<<3)
}

public enum ChainEntityActions {
    case ALLOW
    case DENY
}

class ChainEntityRule: NSObject {
    var ruleDescription: String?
    var participant: ChainEntity.Type
    var operation: [ChainEntityOperations]
    var resource: ChainEntity.Type
    var action: ChainEntityActions
    
    override init() {
        participant = ChainEntity.self
        resource = ChainEntity.self
        operation = [.CREATE, .READ, .UPDATE]
        action = .DENY
    }
}
