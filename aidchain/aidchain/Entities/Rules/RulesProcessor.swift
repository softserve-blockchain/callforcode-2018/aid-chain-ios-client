//
//  RulesProcessor.swift
//  aidchain
//
//  Created by Denys Doronin on 8/27/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class RulesProcessor {

    public class func allowedAssetTypes(for ruleArray: [ChainEntityRule]?, and operation:ChainEntityOperations)->[ChainEntity.Type]? {
        guard let ruleArray = ruleArray else { return nil}
        
        let assetTypes = ruleArray.filter{$0.operation.contains(operation) && $0.action == .ALLOW }.map({(rule)->ChainEntity.Type in return rule.resource})
        
        return assetTypes
    }
}
