//
//  LoginManager.swift
//  aidchain
//
//  Created by Denys Doronin on 9/10/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class User: Codable {
    var login: String?
    var assetTypeString: String?
    var password: String?
}

class LoginManager: Codable {
    var currentLoginId: String?
    var storedUsers: [String:User]?
    
    public class func fetchLoginManager() -> LoginManager {
        if let loginManagerData = Keychain.load(key: loginManagerKey),
            let loginManager = try? JSONDecoder().decode(LoginManager.self, from: loginManagerData) {
            return loginManager
        }
        return LoginManager()
    }
    
    public class func saveLoginManager(loginManager: LoginManager)->Bool {
        if let encoded = try? JSONEncoder().encode(loginManager) {
            return Keychain.save(key: loginManagerKey, data: encoded) == 0
        }
        return false
    }
    
    public class func userAssetType(by userId: String) -> ChainAssets? {
        let loginManager = fetchLoginManager()
        
        if let user = loginManager.storedUsers?[userId] {
            for assetValue in ChainAssets.allValues {
                if assetValue.rawValue == user.assetTypeString {
                    return assetValue
                }
            }
        }
        return nil
    }
}
