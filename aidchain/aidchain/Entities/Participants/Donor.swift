//
//  Donor.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Donor: Person {
    
     override class func entityClassString()-> String {return "Donor" }
    
    var walletAddress: String?
    var balance: Double = 0.0
    
    enum CodingKeys: String, CodingKey {
        case entityId, walletAddress, balance, firstName, lastName
    }
    
    required init() {
        super.init("", "")
        walletAddress = ""
    }
    
    public override func userFilter(assettype: ChainEntity.Type) -> [String:Codable]? {
        
        if assettype == DonateTransaction.self {
            let idstring = Donor.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["donor": idstring]]
        }
        
        return nil
    }
    
    public override func excludedProperties()->[String] {
        return [Donor.CodingKeys.entityId.rawValue]
    }
    
    init( firstName: String, lastName: String, walletAddress:String) {
        super.init(firstName, lastName)
        self.walletAddress = walletAddress
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(walletAddress, forKey: .walletAddress)
        try container.encode(balance, forKey: .balance)
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        walletAddress = try values.decodeIfPresent(String.self, forKey: .walletAddress)
        do {
            balance = try values.decodeIfPresent(Double.self, forKey: .balance) ?? 0.0
        }
        catch {
            if let balanceString = try values.decodeIfPresent(String.self, forKey: .balance) {
                balance = Double(balanceString)!
            }
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Donor].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Donor.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.firstName,
            let _ = self.lastName,
            let _ = self.walletAddress {
            return true
        }
        return false
    }
}
