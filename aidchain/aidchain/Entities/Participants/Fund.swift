//
//  Fund.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Fund: ChainEntity {
     override class func entityClassString()-> String {return "Fund" }
    
    var fundDescription: String?
    var walletAddress: String?
    var balance: Double = 0.0
    var approvedRequests: [Request]?
    
    enum CodingKeys: String, CodingKey {
        case entityId, walletAddress, balance, approvedRequests
        case fundDescription = "description"
    }
    
    required init() {
        super.init()
        fundDescription = ""
        walletAddress = ""
    }
    
    public override func userFilter(assettype: ChainEntity.Type) -> [String:Codable]? {
        
        if assettype == Request.self {
            let idstring = Fund.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["approver": idstring]]
        }
        
        if assettype == ApproveRequestTransaction.self {
            let idstring = Fund.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["approver": idstring]]
        }
        
        return nil
    }
    
    public override func excludedProperties()->[String] {
        return [Fund.CodingKeys.entityId.rawValue, Fund.CodingKeys.approvedRequests.rawValue]
    }
    
    init( fundDescription: String, walletAddress:String, balance: Double) {
        super.init()
        self.walletAddress = walletAddress
        self.fundDescription = fundDescription
        self.balance = balance
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(fundDescription, forKey: .fundDescription)
        try container.encode(walletAddress, forKey: .walletAddress)
        try container.encode(balance, forKey: .balance)
        if let _ = approvedRequests {
            try container.encode(approvedRequests, forKey: .approvedRequests)
        }
        
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        walletAddress = try values.decodeIfPresent(String.self, forKey: .walletAddress)
        fundDescription = try values.decodeIfPresent(String.self, forKey: .fundDescription)
        
        do {
            approvedRequests = try values.decodeIfPresent([Request].self, forKey: .approvedRequests)
        }
        catch {
            if let approvedRequestStrings = try values.decodeIfPresent([String].self, forKey: .approvedRequests) {
                approvedRequests = approvedRequestStrings.map({
                    let entityId = $0.components(separatedBy: Request.resourceWithNamespaceString()+"#").last
                    let request = Request()
                    request.entityId = entityId
                    return request
                })
            }
        }
        
        do {
            balance = try values.decodeIfPresent(Double.self, forKey: .balance) ?? 0.0
        }
        catch {
            if let balanceString = try values.decodeIfPresent(String.self, forKey: .balance) {
                balance = Double(balanceString)!
            }
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Fund].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Fund.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.fundDescription,
            let _ = self.walletAddress {
            return true
        }
        return false
    }
}
