//
//  Inventory.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Inventory: ChainEntity {
     override class func entityClassString()-> String {return "Inventory" }
    
    var locationAddress: String?
    var buckets: [Bucket]?
    
    enum CodingKeys: String, CodingKey {
        case entityId, locationAddress, buckets
    }
    
    required init() {
        super.init()
        locationAddress = ""
    }
    
    public override func userFilter(assettype: ChainEntity.Type) -> [String:Codable]? {
        
        if assettype == InventoryBucketRegistrationTransaction.self {
            let idstring = Inventory.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["targetInventory": idstring]]
        }
        
        return nil
    }
    
    public override func excludedProperties()->[String] {
        return [Inventory.CodingKeys.entityId.rawValue, Inventory.CodingKeys.buckets.rawValue]
    }
    
    init( locationAddress: String) {
        super.init()
        self.locationAddress = locationAddress
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(locationAddress, forKey: .locationAddress)
        if let _ = buckets {
            try container.encode(buckets, forKey: .buckets)
        }
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        locationAddress = try values.decodeIfPresent(String.self, forKey: .locationAddress)
        
        do {
            buckets     = try values.decodeIfPresent([Bucket].self, forKey: .buckets)
        }
        catch {
            if let bucketsSrings = try values.decodeIfPresent([String].self, forKey: .buckets) {
                buckets = bucketsSrings.map({
                    let entityId = $0.components(separatedBy: Bucket.resourceWithNamespaceString()+"#").last
                    let bucket = Bucket()
                    bucket.entityId = entityId
                    return bucket
                })
            }
        }
        
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Inventory].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Inventory.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.locationAddress {
            return true
        }
        return false
    }
}
