//
//  Person.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Person: ChainEntity {
    
    override class func entityClassString()-> String {return "Person"}
    
    var firstName: String?
    var lastName: String?
    
    enum PersonCodingKeys: String, CodingKey {
        case entityId, firstName, lastName
    }
    
    required init() {
        super.init()
        firstName = ""
        lastName = ""
    }
    
    public override func excludedProperties()->[String] {
        return [String]()
    }
    
    init(_ firstName: String, _ lastName: String) {
        super.init()
        self.entityId = "Aid\(Int(Date().timeIntervalSince1970) % 1000)"
        self.firstName = firstName
        self.lastName = lastName
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: PersonCodingKeys.self)
        entityId = try values.decodeIfPresent(String.self, forKey: .entityId)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Person].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Person.self, from: data)
    }
}
