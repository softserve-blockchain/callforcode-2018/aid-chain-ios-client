//
//  Drone.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Location: ChainEntity {
     override class func entityClassString()-> String {return "Location" }
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    enum CodingKeys: String, CodingKey {
        case latitude, longitude
        case entityId = "id"
    }
    
    required init() {
        super.init()
        latitude = 0.0
        longitude = 0.0
    }
    
    init(latitude: Double,longitude:Double ) {
        super.init()
        self.latitude = latitude
        self.longitude = longitude
    }
    
    public override func excludedProperties()->[String] {
        return [String]()
    }
    
    override func isConsistentToPush() -> Bool {
        return true
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
    }
}

class Drone: ChainEntity {
     override class func entityClassString()-> String {return "Drone" }
    
    var modelType: String?
    var targetBucket: Bucket?
    var recentLocation: Location?
    
    enum CodingKeys: String, CodingKey {
        case entityId, modelType, targetBucket, recentLocation
    }
    
    required init() {
        super.init()
        modelType = ""
        targetBucket = nil
        recentLocation = Location(latitude: 0.0, longitude: 0.0)
    }
    
    init( modelType: String, targetBucket: Bucket?, recentLocation:Location) {
        super.init()
        self.modelType = modelType
        self.targetBucket = targetBucket
        self.recentLocation = recentLocation
    }
    
    override func setupEntity() {
        super.setupEntity()
        recentLocation = Location(latitude: 0.0, longitude: 0.0)
    }
    
    public override func userFilter(assettype: ChainEntity.Type) -> [String:Codable]? {
        
        if assettype == StartBucketDeliveryTransaction.self {
            let idstring = Drone.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["targetDrone": idstring]]
        }
        
        if assettype == FinishBucketDeliveryTransaction.self {
            let idstring = Drone.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["targetDrone": idstring]]
        }
        
        return nil
    }
    
    public override func excludedProperties() -> [String] {
        return [Drone.CodingKeys.entityId.rawValue,
                Drone.CodingKeys.targetBucket.rawValue,
                Drone.CodingKeys.recentLocation.rawValue]
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Drone].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Drone.self, from: data)
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(modelType, forKey: .modelType)
        if let _ = targetBucket {
            try container.encode(targetBucket, forKey: .targetBucket)
        }
        try container.encode(recentLocation, forKey: .recentLocation)

    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        modelType = try values.decodeIfPresent(String.self, forKey: .modelType)
        
        do {
            targetBucket = try values.decodeIfPresent(Bucket.self, forKey: .targetBucket)
        }
        catch {
            if let targetBucketString = try values.decodeIfPresent(String.self, forKey: .targetBucket) {
                
                let entityId = targetBucketString.components(separatedBy: Bucket.resourceWithNamespaceString()+"#").last
                targetBucket = Bucket()
                targetBucket?.entityId = entityId
            }
        }
        
        do {
            recentLocation = try values.decodeIfPresent(Location.self, forKey: .recentLocation)
        }
        catch {
            if let recentLocationString = try values.decodeIfPresent(String.self, forKey: .recentLocation) {
                let array = recentLocationString.split(separator: ";")
                if array.count == 2 {
                    recentLocation = Location(latitude: Double(array[0]) ?? 0.0, longitude: Double(array[1]) ?? 0.0)
                }
            }
        }
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.modelType,
            let _ = self.recentLocation {
            return true
        }
        return false
    }
}
