//
//  AidSeeker.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Seeker: Person {
     override class func entityClassString()-> String {return "AidSeeker" }
    
    var requests: [Request]?
    
    enum CodingKeys: String, CodingKey {
        case entityId, firstName, lastName, requests
    }
    
    required init() {
        super.init("", "")
    }
    
    override init(_ firstName: String, _ lastName: String) {
        super.init(firstName,lastName)
    }
    
    public override func userFilter(assettype: ChainEntity.Type) -> [String:Codable]? {
        
        if assettype == Request.self {
            let idstring = Seeker.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["seeker": idstring]]
        }
        
        if assettype == OpenDonateTransaction.self {
            let idstring = Seeker.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["initiator": idstring]]
        }
        return nil
    }
    
    public override func excludedProperties()->[String] {
        return [Seeker.CodingKeys.entityId.rawValue, Seeker.CodingKeys.requests.rawValue]
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Seeker].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Seeker.self, from: data)
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        if let _ = requests {
            try container.encode(requests, forKey: .requests)
        } 
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            requests = try values.decodeIfPresent([Request].self, forKey: .requests)
        }
        catch {
            if let requestSrings = try values.decodeIfPresent([String].self, forKey: .requests) {
                requests = requestSrings.map({
                    let entityId = $0.components(separatedBy: Request.resourceWithNamespaceString()+"#").last
                    let request = Request(entityId: entityId ?? "unknownID")
                    return request
                })
            }
        }
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.firstName,
            let _ = self.lastName {
            return true
        }
        return false
    }
}
