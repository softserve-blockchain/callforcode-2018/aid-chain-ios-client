//
//  ExchangeProcessor.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ExchangeProcessor: ChainEntity {
     override class func entityClassString()-> String {return "ExchangeProcessor" }
    
    var bucketsToProcess: [Bucket]?
    
    enum CodingKeys: String, CodingKey {
        case entityId, bucketsToProcess
    }
    
    required init() {
        super.init()
    }
    
    public override func excludedProperties()->[String] {
        return [ExchangeProcessor.CodingKeys.bucketsToProcess.rawValue]
    }
    
    override func setupEntity() {
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(entityId, forKey: .entityId)
        if let _ = bucketsToProcess {
            try container.encode(bucketsToProcess, forKey: .bucketsToProcess)
        }
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        entityId = try values.decodeIfPresent(String.self, forKey: .entityId)
        
        do {
            bucketsToProcess     = try values.decodeIfPresent([Bucket].self, forKey: .bucketsToProcess)
        }
        catch {
            if let bucketsSrings = try values.decodeIfPresent([String].self, forKey: .bucketsToProcess) {
                bucketsToProcess = bucketsSrings.map({
                    let entityId = $0.components(separatedBy: Bucket.resourceWithNamespaceString()+"#").last
                    let bucket = Bucket()
                    bucket.entityId = entityId
                    return bucket
                })
            }
        }
        
    }
    
    public override func userFilter(assettype: ChainEntity.Type) -> [String:Codable]? {
        
        if assettype == PurchaseBucketTransaction.self {
            let idstring = ExchangeProcessor.resourceWithNamespaceString()+"#" + (self.entityId ?? "")
            return ["where":["processor": idstring]]
        }
        
        return nil
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([ExchangeProcessor].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(ExchangeProcessor.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId {
            return true
        }
        return false
    }
}
