//
//  AidParticipant.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ChainEntity: NSObject, Codable {
    
    class func entityClassString()-> String {return "AidChainEntity"}
    
    public var entityId: String?
    
    required override init() {
        self.entityId = ChainUtilities.generateIDString()
    }
    
    public func userFilter(assettype: ChainEntity.Type) -> [String:Codable]? {
        return nil
    }
    
    public func excludedProperties()->[String] {
        return [String]()
    }
    
    public func isConsistentToPush() -> Bool {
        return false
    }
    
    public class func resourceWithNamespaceString()-> String {
        return assetsNamespace + self.entityClassString()
    }
    
    public func buildRequest(urlString: String, httpMethod: String)-> URLRequest? {
        guard let serviceUrl = URL(string: urlString) else { return nil }
        
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = httpMethod
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONEncoder().encode(self) else { return nil }
        
        request.httpBody = httpBody
        
        return request
    }
    
    func updateEntityRequest(_ endpointURLString: String!)-> URLRequest? {
        if self.isConsistentToPush() == true {
            let urlString = String(format: endpointURLString + "/" + (self.entityId ?? ""))
            let storedId = self.entityId
            defer {
                self.entityId = storedId
            }
            
            self.entityId = nil
        
            return buildRequest(urlString: urlString, httpMethod: "PUT")
        }
        return nil
    }
    
    public func createEntityRequest(_ endpointURLString: String!) -> URLRequest? {
        if self.isConsistentToPush() == true {
            return buildRequest(urlString: endpointURLString, httpMethod:"POST")
        }
        return nil
    }
    
    public class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([ChainEntity].self, from: data)
    }
    
    public class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(ChainEntity.self, from: data)
    }
    
    //Example how to decode date from rest server
    //    if let dateString =  try values.decodeIfPresent(String.self, forKey: .timestamp),
    //    let date = Transaction.dateFormatter().date(from: dateString) {
    //        timestamp = date
    //    }
    static func dateFormatter() -> ISO8601DateFormatter {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        
        return formatter
    }

    public func setupEntity() {
        self.entityId = ChainUtilities.generateIDString()
    }
}
