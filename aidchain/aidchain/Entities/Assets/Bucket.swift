//
//  AidBucket.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

enum AidBucketStatus {
    case CREATED
    case PURCHASED
    case INVENTORY
    case INPROGRESS
    case DELIVERED
}

class Bucket: ChainEntity {
    
    override class func entityClassString()-> String {return "AidBucket" }
    
    var aidRequest: Request?
    var commodities: [Commodity]?
    var bucketStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case entityId, aidRequest, commodities, bucketStatus
    }
    
    required init() {
        super.init()
        aidRequest = Request(entityId: "")
        commodities = [Commodity()]
        bucketStatus = "CREATED"
    }
    
    override func setupEntity() {
        super.setupEntity()
        self.bucketStatus = "CREATED"
    }
    
    public override func excludedProperties()->[String] {
        return [Bucket.CodingKeys.entityId.rawValue,Bucket.CodingKeys.bucketStatus.rawValue]
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Bucket].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Bucket.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.aidRequest,
            let _ = self.commodities {
            return true
        }
        return false
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(bucketStatus, forKey: .bucketStatus)
        
        if let entityId = aidRequest?.entityId {
            try container.encode(Request.resourceWithNamespaceString()+"#"+entityId, forKey: .aidRequest)
        }
        
        if let comodities = commodities {
            
            let mappedComodities = comodities.map({
                return Commodity.resourceWithNamespaceString()+"#"+$0.entityId!
            })
            try container.encode(mappedComodities, forKey: .commodities)
        }
      
    }
    
    fileprivate func decodeCommodities(_ comoditiesStrings: [String]) {
        commodities = comoditiesStrings.map({
            let entityId = $0.components(separatedBy: Commodity.resourceWithNamespaceString()+"#").last
            let commodity = Commodity()
            commodity.entityId = entityId
            return commodity
        })
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        bucketStatus = try values.decodeIfPresent(String.self, forKey: .bucketStatus)
        
        do {
            aidRequest = try values.decodeIfPresent(Request.self, forKey: .aidRequest)
        }
        catch {
            if let aidRequestString = try values.decodeIfPresent(String.self, forKey: .aidRequest) {
                
                let entityId = aidRequestString.components(separatedBy: Request.resourceWithNamespaceString()+"#").last
                aidRequest = Request(entityId: entityId ?? "unknown")
            }
        }
        
        do {
            if let comoditiesStrings = try values.decodeIfPresent([String].self, forKey: .commodities) {
                decodeCommodities(comoditiesStrings)
            }
        }
        catch {
            if let comoditiesStrings = try values.decodeIfPresent(String.self, forKey: .commodities) {
                decodeCommodities(comoditiesStrings.components(separatedBy: ";"))
            }
        }
        
    }
}
