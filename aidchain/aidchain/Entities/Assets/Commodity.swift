//
//  AidCommodity.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Commodity: ChainEntity {

     override class func entityClassString()-> String {return "AidCommodity" }
    
    var commodityType: String?
    var commodityDescription: String?
    var purchasePrice: Double = 0.0
    var weightKg: Double = 0.0
    
    enum CodingKeys: String, CodingKey {
        case entityId, commodityType, commodityDescription, purchasePrice, weightKg
    }
    
    required init() {
        super.init()
        commodityType = ""
        commodityDescription = ""
    }
    
    public override func excludedProperties()->[String] {
        return [Commodity.CodingKeys.entityId.rawValue]
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Commodity].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Commodity.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.commodityType,
            let _ = self.commodityDescription {
            return true
        }
        return false
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(commodityType, forKey: .commodityType)
        try container.encode(commodityDescription, forKey: .commodityDescription)
        try container.encode(purchasePrice, forKey: .purchasePrice)
        try container.encode(weightKg, forKey: .weightKg)
        
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        commodityType = try values.decodeIfPresent(String.self, forKey: .commodityType)
        commodityDescription = try values.decodeIfPresent(String.self, forKey: .commodityDescription)
        
        do {
            purchasePrice = try values.decodeIfPresent(Double.self, forKey: .purchasePrice) ?? 0.0
        }
        catch {
            if let purchasePriceString = try values.decodeIfPresent(String.self, forKey: .purchasePrice) {
                purchasePrice = Double(purchasePriceString)!
            }
        }
        
        do {
            weightKg = try values.decodeIfPresent(Double.self, forKey: .weightKg) ?? 0.0
        }
        catch {
            if let weightKgString = try values.decodeIfPresent(String.self, forKey: .weightKg) {
                weightKg = Double(weightKgString)!
            }
        }
    }
    
}
