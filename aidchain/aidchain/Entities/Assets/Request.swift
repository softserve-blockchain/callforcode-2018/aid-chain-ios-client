//
//  AidRequest.swift
//  aidchain
//
//  Created by Denys Doronin on 8/15/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class Request: ChainEntity {
    
    override class func entityClassString()-> String {return "AidRequest" }

    var aiddescription: String?
    var requestStatus: String?
    var donors: [Donor]?
    var seeker: Seeker?
    var approver: Fund?
    var requiredBalance:Double?
    var currentBalance: Double?
    var aidBuckets: [Bucket]?
    var targetLocation: Location?
    var creationTime: Date?
    var deliveryTime: Date?
    
    enum CodingKeys: String, CodingKey {
        case requestStatus, donors, seeker,approver, requiredBalance, currentBalance, aidBuckets, entityId, targetLocation, creationTime, deliveryTime
        case aiddescription = "description"
    }
    
    required init() {
        super.init()
        self.requestStatus = "CREATED"
        self.requiredBalance = 0.0
        self.currentBalance = 0.0
        self.targetLocation = Location(latitude: 0.0, longitude: 0.0)
        self.creationTime = Date()
    }
    
    public override func excludedProperties()->[String] {
        return [Request.CodingKeys.entityId.rawValue,
                Request.CodingKeys.donors.rawValue,
                Request.CodingKeys.approver.rawValue,
                Request.CodingKeys.requestStatus.rawValue,
                Request.CodingKeys.aidBuckets.rawValue,
                Request.CodingKeys.currentBalance.rawValue,
                Request.CodingKeys.creationTime.rawValue,
                Request.CodingKeys.deliveryTime.rawValue
        ]
    }
    
    override func setupEntity() {
        super.setupEntity()
        self.requestStatus = "CREATED"
        self.currentBalance = 0.0
        self.creationTime = Date()
    }
    
    init(entityId: String) {
        super.init()
        self.entityId = entityId
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([Request].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(Request.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.aiddescription,
            let _ = self.requiredBalance,
            let _ = self.seeker,
            let _ = self.targetLocation {
            return true
        }
        return false
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(entityId, forKey: .entityId)
        try container.encode(aiddescription, forKey: .aiddescription)
        try container.encode(requestStatus, forKey: .requestStatus)
        try container.encode(requiredBalance, forKey: .requiredBalance)
        try container.encode(currentBalance, forKey: .currentBalance)

        if let targetLocation = targetLocation {
            try container.encode(targetLocation, forKey: .targetLocation)
        }
        
        if let entityId = seeker?.entityId {
            try container.encode(Seeker.resourceWithNamespaceString()+"#"+entityId, forKey: .seeker)
        }
        
        if let approverId = approver?.entityId {
            try container.encode(Fund.resourceWithNamespaceString()+"#"+approverId, forKey: .approver)
        }
        
        if let donors = donors {
            let mappedDonors = donors.map({
                return Donor.resourceWithNamespaceString()+"#"+$0.entityId!
            })
            try container.encode(mappedDonors, forKey: .donors)
        }
        
        if let aidBuckets = aidBuckets {
            let mappedBuckets = aidBuckets.map({
                return Bucket.resourceWithNamespaceString()+"#"+$0.entityId!
            })
            try container.encode(mappedBuckets, forKey: .aidBuckets)
        }
        
        if  let creationTime =  creationTime {
            let dateString = ChainEntity.dateFormatter().string(from: creationTime)
            try container.encode(dateString, forKey: .creationTime)
        }
        
        if  let deliveryTime =  deliveryTime {
            let dateString = ChainEntity.dateFormatter().string(from: deliveryTime)
            try container.encode(dateString, forKey: .deliveryTime)
        }
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        aiddescription = try values.decodeIfPresent(String.self, forKey: .aiddescription)
        requestStatus  = try values.decodeIfPresent(String.self, forKey: .requestStatus)
        
        do {
            donors     = try values.decodeIfPresent([Donor].self, forKey: .donors)
        } catch {
            if let donorStrings = try values.decodeIfPresent([String].self, forKey: .donors) {
                donors = donorStrings.map({
                    let entityId = $0.components(separatedBy: Donor.resourceWithNamespaceString()+"#").last
                    let donor = Donor()
                    donor.entityId = entityId
                    return donor
                })
            }
        }
        
        do {
            aidBuckets = try values.decodeIfPresent([Bucket].self, forKey: .aidBuckets)
        } catch {
            if let bucketsSrings = try values.decodeIfPresent([String].self, forKey: .aidBuckets) {
                aidBuckets = bucketsSrings.map({
                    let entityId = $0.components(separatedBy: Bucket.resourceWithNamespaceString()+"#").last
                    let bucket = Bucket()
                    bucket.entityId = entityId
                    return bucket
                })
            }
        }
        
        do {
            targetLocation = try values.decodeIfPresent(Location.self, forKey: .targetLocation)
        }
        catch {
            
            if let targetLocationString = try values.decodeIfPresent(String.self, forKey: .targetLocation) {
                let locationArray = targetLocationString.components(separatedBy: ";")
                if locationArray.count == 2 {
                    let latitude = Double(locationArray.first ?? "0.0")
                    let longitude = Double(locationArray.last ?? "0.0")
                    targetLocation = Location(latitude: latitude ?? 0.0, longitude: longitude ?? 0.0)
                }
            }
            
        }
        
        do {
            requiredBalance = try values.decodeIfPresent(Double.self, forKey: .requiredBalance) ?? 0.0
        }
        catch {
            if let requiredBalanceString = try values.decodeIfPresent(String.self, forKey: .requiredBalance) {
                requiredBalance = Double(requiredBalanceString)!
            }
        }
        
        do {
            currentBalance = try values.decodeIfPresent(Double.self, forKey: .currentBalance) ?? 0.0
        }
        catch {
            if let currentBalanceString = try values.decodeIfPresent(String.self, forKey: .currentBalance) {
                currentBalance = Double(currentBalanceString)!
            }
        }
        
        do {
            seeker = try values.decodeIfPresent(Seeker.self, forKey: .seeker)
        } catch {
            if let seekerString = try values.decodeIfPresent(String.self, forKey: .seeker) {
                let entityId = seekerString.components(separatedBy: Seeker.resourceWithNamespaceString()+"#").last
                seeker = Seeker()
                seeker?.entityId = entityId
            }
        }
        
        do {
            approver = try values.decodeIfPresent(Fund.self, forKey: .approver)
        } catch {
            if let approverString = try values.decodeIfPresent(String.self, forKey: .approver) {
                let entityId = approverString.components(separatedBy: Fund.resourceWithNamespaceString()+"#").last
                approver = Fund()
                approver?.entityId = entityId
            }
        }
        
        if let dateString =  try values.decodeIfPresent(String.self, forKey: .creationTime),
            let date = ChainEntity.dateFormatter().date(from: dateString) {
            creationTime = date
        }
        
        if let dateString =  try values.decodeIfPresent(String.self, forKey: .deliveryTime),
            let date = ChainEntity.dateFormatter().date(from: dateString) {
            deliveryTime = date
        }
        
    }
    
}
