//
//  Constants.swift
//  Fraud-detection
//
//  Created by Denys Doronin on 6/14/18.
//  Copyright © 2018 Doro. All rights reserved.
//

import Foundation

let assetsNamespace = "resource:org.aid.processing."

let currentUserId = "currentUserId"
let currentUserPassword = "currentUserPassword"
let loginManagerKey = "loginManagerKey"

enum ChainAssets: String {
    case AidSeeker = "Aid Seeker"
    case AidDonor = "Donor"
    case AidFund = "Fund"
    case AidDrone = "Drone"
    case AidInventory = "Inventory"
    case AidExchangeProcessor = "Exchange processor"
    
    case AidCommodity = "Aid commodity"
    case AidBucket = "Aid bucket"
    case AidRequest = "Aid request"
    
    case AidOpenDonateTransaction = "Open donate"
    case AidApproveRequestTransaction = "Approve request"
    case AidDonateTransaction = "Donate"
    case AidPurchaseBucketTransaction = "Purchase bucket"
    case AidInventoryBucketRegistrationTransaction = "Inventory bucket registration"
    case AidStartBucketDeliveryTransaction = "Start bucket delivery"
    case AidFinishBucketDeliveryTransaction = "Finish bucket delivery"
    
    static let allParticipants = [ChainAssets.AidSeeker,
                                  ChainAssets.AidDonor,
                                  ChainAssets.AidFund,
                                  ChainAssets.AidDrone,
                                  ChainAssets.AidInventory,
                                  ChainAssets.AidExchangeProcessor
                                 ]
    
    static let allAssets = [ChainAssets.AidCommodity,
                            ChainAssets.AidBucket,
                            ChainAssets.AidRequest
                            ]
    
    static let allTransactions = [ChainAssets.AidOpenDonateTransaction,
                                  ChainAssets.AidApproveRequestTransaction,
                                  ChainAssets.AidDonateTransaction,
                                  ChainAssets.AidPurchaseBucketTransaction,
                                  ChainAssets.AidInventoryBucketRegistrationTransaction,
                                  ChainAssets.AidStartBucketDeliveryTransaction,
                                  ChainAssets.AidFinishBucketDeliveryTransaction];
    
    static let allValues = [ChainAssets.AidSeeker,
                            ChainAssets.AidDonor,
                            ChainAssets.AidFund,
                            ChainAssets.AidDrone,
                            ChainAssets.AidInventory,
                            ChainAssets.AidExchangeProcessor,
                            ChainAssets.AidCommodity,
                            ChainAssets.AidBucket,
                            ChainAssets.AidRequest,
                            ChainAssets.AidOpenDonateTransaction,
                            ChainAssets.AidApproveRequestTransaction,
                            ChainAssets.AidDonateTransaction,
                            ChainAssets.AidPurchaseBucketTransaction,
                            ChainAssets.AidInventoryBucketRegistrationTransaction,
                            ChainAssets.AidStartBucketDeliveryTransaction,
                            ChainAssets.AidFinishBucketDeliveryTransaction
                            ]
    

}

func assetToType(asset: ChainAssets)->ChainEntity.Type {
    switch asset {
        
    case ChainAssets.AidSeeker:
        return Seeker.self
    case .AidDonor:
        return Donor.self
    case .AidFund:
        return Fund.self
    case .AidDrone:
        return Drone.self
    case .AidInventory:
        return Inventory.self
    case .AidExchangeProcessor:
        return ExchangeProcessor.self
    case .AidCommodity:
        return Commodity.self
    case .AidBucket:
        return Bucket.self
    case .AidRequest:
        return Request.self
    case .AidApproveRequestTransaction:
        return ApproveRequestTransaction.self
    case .AidDonateTransaction:
        return DonateTransaction.self
    case .AidPurchaseBucketTransaction:
        return PurchaseBucketTransaction.self
    case .AidInventoryBucketRegistrationTransaction:
        return InventoryBucketRegistrationTransaction.self
    case .AidStartBucketDeliveryTransaction:
        return StartBucketDeliveryTransaction.self
    case .AidFinishBucketDeliveryTransaction:
        return FinishBucketDeliveryTransaction.self
    case .AidOpenDonateTransaction:
        return OpenDonateTransaction.self
    }
}

func typeToAsset(type: ChainEntity.Type)->ChainAssets? {
    switch type {
    case is Seeker.Type:
        return .AidSeeker
    case is Donor.Type:
        return .AidDonor
    case is Fund.Type:
        return .AidFund
    case is Drone.Type:
        return .AidDrone
    case is Inventory.Type:
        return .AidInventory
    case is ExchangeProcessor.Type:
        return .AidExchangeProcessor
    case is Commodity.Type:
        return .AidCommodity
    case is Bucket.Type:
        return .AidBucket
    case is Request.Type:
        return  .AidRequest
    case is OpenDonateTransaction.Type:
        return .AidOpenDonateTransaction
    case is ApproveRequestTransaction.Type:
        return .AidApproveRequestTransaction
    case is DonateTransaction.Type:
        return .AidDonateTransaction
    case is PurchaseBucketTransaction.Type:
        return .AidPurchaseBucketTransaction
    case is InventoryBucketRegistrationTransaction.Type:
        return .AidInventoryBucketRegistrationTransaction
    case is StartBucketDeliveryTransaction.Type:
        return .AidStartBucketDeliveryTransaction
    case is FinishBucketDeliveryTransaction.Type:
        return .AidFinishBucketDeliveryTransaction
    default:
        return nil
    }
}
