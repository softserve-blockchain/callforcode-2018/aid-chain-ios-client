//
//  OpenDonateTransaction.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class OpenDonateTransaction: ChainTransaction {

     override class func entityClassString()-> String {return "OpenDonateTransaction" }
    
    var asset: Request?
    var initiator: Seeker?
    
    enum CodingKeys: String, CodingKey {
        case entityId = "transactionId"
        case timestamp,asset,initiator
    }
    
    public override func excludedProperties()->[String] {
        return [OpenDonateTransaction.CodingKeys.entityId.rawValue,
                OpenDonateTransaction.CodingKeys.timestamp.rawValue]
    }
    
    override func setupEntity() {
        super.setupEntity()
    }
    
    required init() {
        super.init()
        asset = Request()
        initiator = Seeker()
    }
    
    override func encode(to encoder: Encoder) throws {
        
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if let asset =  asset, let entityId = asset.entityId {
            try container.encode(assetsNamespace + Request.entityClassString() + "#" + entityId, forKey: .asset)
        }
        
        if let initiator =  initiator, let entityId = initiator.entityId {
            try container.encode(assetsNamespace + Seeker.entityClassString() + "#" + entityId, forKey: .initiator)
        }
    }
    
    required init(from decoder: Decoder) throws {
        
        try super.init(from: decoder)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let assetString = try values.decodeIfPresent(String.self, forKey: .asset) {
            let entityId = assetString.components(separatedBy: Request.resourceWithNamespaceString()+"#").last
            asset = Request()
            asset?.entityId = entityId
        }
        
        if let seekerString = try values.decodeIfPresent(String.self, forKey: .initiator) {
            let entityId = seekerString.components(separatedBy: Seeker.resourceWithNamespaceString()+"#").last
            initiator = Seeker()
            initiator?.entityId = entityId
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([OpenDonateTransaction].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(OpenDonateTransaction.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.asset,
            let _ = self.initiator {
            return true
        }
        return false
    }
}
