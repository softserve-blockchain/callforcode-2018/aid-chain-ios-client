//
//  ApproveRequestTransaction.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ApproveRequestTransaction: ChainTransaction {

    override class func entityClassString()-> String {return "ApproveRequestTransaction" }
    
    var asset: Request?
    var approver: Fund?
    
    enum CodingKeys: String, CodingKey {
        case entityId = "transactionId"
        case timestamp,asset,approver
    }
    
    public override func excludedProperties()->[String] {
        return [ApproveRequestTransaction.CodingKeys.entityId.rawValue,
                ApproveRequestTransaction.CodingKeys.timestamp.rawValue]
    }
    
    override func setupEntity() {
        super.setupEntity()
    }
    
    required init() {
        super.init()
        asset = Request()
        approver = Fund()
    }
    
    override func encode(to encoder: Encoder) throws {
        
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if let asset =  asset, let entityId = asset.entityId {
            try container.encode(assetsNamespace + Request.entityClassString() + "#" + entityId, forKey: .asset)
        }
        
        if let approver =  approver, let entityId = approver.entityId {
            try container.encode(assetsNamespace + Fund.entityClassString() + "#" + entityId, forKey: .approver)
        }
    }
    
    required init(from decoder: Decoder) throws {
        
        try super.init(from: decoder)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let assetString = try values.decodeIfPresent(String.self, forKey: .asset) {
            let entityId = assetString.components(separatedBy: Request.resourceWithNamespaceString()+"#").last
            asset = Request()
            asset?.entityId = entityId
        }
        
        if let approverString = try values.decodeIfPresent(String.self, forKey: .approver) {
            let entityId = approverString.components(separatedBy: Fund.resourceWithNamespaceString()+"#").last
            approver = Fund()
            approver?.entityId = entityId
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([ApproveRequestTransaction].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(ApproveRequestTransaction.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.asset,
            let _ = self.approver {
            return true
        }
        return false
    }
}
