//
//  PurchaseBucketTransaction.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class PurchaseBucketTransaction: ChainTransaction {
    
     override class func entityClassString()-> String {return "PurchaseBucketTransaction" }
    
    var asset: Bucket?
    var processor: ExchangeProcessor?
    
    enum CodingKeys: String, CodingKey {
        case entityId = "transactionId"
        case timestamp,asset,processor
    }
    
    public override func excludedProperties()->[String] {
        return [PurchaseBucketTransaction.CodingKeys.entityId.rawValue,
                PurchaseBucketTransaction.CodingKeys.timestamp.rawValue]
    }
    
    override func setupEntity() {
        super.setupEntity()
    }
    
    required init() {
        super.init()
        asset = Bucket()
        processor = ExchangeProcessor()
    }
    
    override func encode(to encoder: Encoder) throws {
        
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if let asset =  asset, let entityId = asset.entityId {
            try container.encode(assetsNamespace + Bucket.entityClassString() + "#" + entityId, forKey: .asset)
        }
        
        if let processor =  processor, let entityId = processor.entityId {
            try container.encode(assetsNamespace + ExchangeProcessor.entityClassString() + "#" + entityId, forKey: .processor)
        }
    }
    
    required init(from decoder: Decoder) throws {
        
        try super.init(from: decoder)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let assetString = try values.decodeIfPresent(String.self, forKey: .asset) {
            let entityId = assetString.components(separatedBy: Bucket.resourceWithNamespaceString()+"#").last
            asset = Bucket()
            asset?.entityId = entityId
        }
        
        if let processorString = try values.decodeIfPresent(String.self, forKey: .processor) {
            let entityId = processorString.components(separatedBy: ExchangeProcessor.resourceWithNamespaceString()+"#").last
            processor = ExchangeProcessor()
            processor?.entityId = entityId
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([PurchaseBucketTransaction].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(PurchaseBucketTransaction.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.asset,
            let _ = self.processor {
            return true
        }
        return false
    }
}
