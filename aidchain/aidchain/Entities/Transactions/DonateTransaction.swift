//
//  DonateTransaction.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class DonateTransaction: ChainTransaction {

     override class func entityClassString()-> String {return "DonateTransaction" }
    
    var destination:Fund?
    var donor: Donor?
    var amount: Double = 0.0
    
    enum CodingKeys: String, CodingKey {
        case entityId = "transactionId"
        case timestamp,destination,donor,amount
    }
    
    public override func excludedProperties()->[String] {
        return [DonateTransaction.CodingKeys.entityId.rawValue,
                DonateTransaction.CodingKeys.timestamp.rawValue]
    }
    
    override func setupEntity() {
        super.setupEntity()
    }
    
    required init() {
        super.init()
        destination = Fund()
        donor = Donor()
    }
    
    override func encode(to encoder: Encoder) throws {
        
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if let destination =  destination, let entityId = destination.entityId {
            try container.encode(assetsNamespace + Fund.entityClassString() + "#" + entityId, forKey: .destination)
        }
        
        if let donor =  donor, let entityId = donor.entityId {
            try container.encode(assetsNamespace + Donor.entityClassString() + "#" + entityId, forKey: .donor)
        }
        
        try container.encode(amount, forKey: .amount)
       
    }
    
    required init(from decoder: Decoder) throws {
        
        try super.init(from: decoder)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            amount = try values.decodeIfPresent(Double.self, forKey: .amount) ?? 0.0
        }
        catch {
            if let amountString = try values.decodeIfPresent(String.self, forKey: .amount) {
                amount = Double(amountString)!
            }
        }
        
        if let destinationString = try values.decodeIfPresent(String.self, forKey: .destination) {
            let entityId = destinationString.components(separatedBy: Fund.resourceWithNamespaceString()+"#").last
            destination = Fund()
            destination?.entityId = entityId
        }
        
        if let donorString = try values.decodeIfPresent(String.self, forKey: .donor) {
            let entityId = donorString.components(separatedBy: Donor.resourceWithNamespaceString()+"#").last
            donor = Donor()
            donor?.entityId = entityId
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([DonateTransaction].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(DonateTransaction.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.destination,
            let _ = self.donor {
            return true
        }
        return false
    }
}
