//
//  ChainTransaction.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ChainTransaction: ChainEntity {
     var timestamp: Date? = nil
    
    enum ChainTransactionKeys: String, CodingKey {
        case entityId = "transactionId"
        case timestamp
    }
    override func encode(to encoder: Encoder) throws {
        
    //    var container = encoder.container(keyedBy: ChainTransactionKeys.self)
        
//        if let transactionId =  entityId {
//            try container.encode(transactionId, forKey: .entityId)
//        }
//        if  let timestamp =  timestamp {
//            let dateString = ChainEntity.dateFormatter().string(from: timestamp)
//            try container.encode(dateString, forKey: .timestamp)
//        }
    }
    
    required init(from decoder: Decoder) throws {
        
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: ChainTransactionKeys.self)
        
        entityId = try values.decodeIfPresent(String.self, forKey: .entityId)
        
        if let dateString =  try values.decodeIfPresent(String.self, forKey: .timestamp),
            let date = ChainEntity.dateFormatter().date(from: dateString) {
            timestamp = date
        }
        
    }
    
    required init() {
        super.init()
        timestamp = Date(timeIntervalSinceNow: 0)
        
    }
}
