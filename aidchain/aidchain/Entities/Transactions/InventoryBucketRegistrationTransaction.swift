//
//  InventoryBucketRegistrationTransaction.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class InventoryBucketRegistrationTransaction: ChainTransaction {
    
     override class func entityClassString()-> String {return "InventoryBucketRegistrationTransaction" }
    
    var asset: Bucket?
    var targetInventory: Inventory?
    
    enum CodingKeys: String, CodingKey {
        case entityId = "transactionId"
        case timestamp,asset,targetInventory
    }
    
    public override func excludedProperties()->[String] {
        return [InventoryBucketRegistrationTransaction.CodingKeys.entityId.rawValue,
                InventoryBucketRegistrationTransaction.CodingKeys.timestamp.rawValue]
    }
    
    override func setupEntity() {
        super.setupEntity()
    }
    
    required init() {
        super.init()
        asset = Bucket()
        targetInventory = Inventory()
    }
    
    override func encode(to encoder: Encoder) throws {
        
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if let asset =  asset, let entityId = asset.entityId {
            try container.encode(assetsNamespace + Bucket.entityClassString() + "#" + entityId, forKey: .asset)
        }
        
        if let targetInventory =  targetInventory, let entityId = targetInventory.entityId {
            try container.encode(assetsNamespace + Inventory.entityClassString() + "#" + entityId, forKey: .targetInventory)
        }
    }
    
    required init(from decoder: Decoder) throws {
        
        try super.init(from: decoder)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let assetString = try values.decodeIfPresent(String.self, forKey: .asset) {
            let entityId = assetString.components(separatedBy: Bucket.resourceWithNamespaceString()+"#").last
            asset = Bucket()
            asset?.entityId = entityId
        }
        
        if let targetInventoryString = try values.decodeIfPresent(String.self, forKey: .targetInventory) {
            let entityId = targetInventoryString.components(separatedBy: Inventory.resourceWithNamespaceString()+"#").last
            targetInventory = Inventory()
            targetInventory?.entityId = entityId
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([InventoryBucketRegistrationTransaction].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(InventoryBucketRegistrationTransaction.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.asset,
            let _ = self.targetInventory {
            return true
        }
        return false
    }
}
