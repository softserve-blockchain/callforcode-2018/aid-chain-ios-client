//
//  TransactionProcessor.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class TransactionProcessor {
    
    public class func allowedTransactionsTypes(for role: ChainAssets)->[ChainEntity.Type]? {
        switch role {
            
        case ChainAssets.AidSeeker:
            return [OpenDonateTransaction.self]
        case .AidDonor:
            return [DonateTransaction.self]
        case .AidFund:
            return [ApproveRequestTransaction.self]
        case .AidDrone:
            return [StartBucketDeliveryTransaction.self, FinishBucketDeliveryTransaction.self]
        case .AidInventory:
            return [InventoryBucketRegistrationTransaction.self]
        case .AidExchangeProcessor:
            return [PurchaseBucketTransaction.self]
        default:
            return nil
        }
    }
}
