//
//  FinishBucketDeliveryTransaction.swift
//  aidchain
//
//  Created by Denys Doronin on 9/3/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class FinishBucketDeliveryTransaction: ChainTransaction {
    
     override class func entityClassString()-> String {return "FinishBucketDeliveryTransaction" }
    
    var asset: Bucket?
    var targetDrone: Drone?
    
    enum CodingKeys: String, CodingKey {
        case entityId = "transactionId"
        case timestamp,asset,targetDrone
    }
    
    public override func excludedProperties()->[String] {
        return [FinishBucketDeliveryTransaction.CodingKeys.entityId.rawValue,
                FinishBucketDeliveryTransaction.CodingKeys.timestamp.rawValue]
    }
    
    override func setupEntity() {
        super.setupEntity()
    }
    
    required init() {
        super.init()
        asset = Bucket()
        targetDrone = Drone()
    }
    
    override func encode(to encoder: Encoder) throws {
        
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if let asset =  asset, let entityId = asset.entityId {
            try container.encode(assetsNamespace + Bucket.entityClassString() + "#" + entityId, forKey: .asset)
        }
        
        if let targetDrone =  targetDrone, let entityId = targetDrone.entityId {
            try container.encode(assetsNamespace + Drone.entityClassString() + "#" + entityId, forKey: .targetDrone)
        }
    }
    
    required init(from decoder: Decoder) throws {
        
        try super.init(from: decoder)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let assetString = try values.decodeIfPresent(String.self, forKey: .asset) {
            let entityId = assetString.components(separatedBy: Bucket.resourceWithNamespaceString()+"#").last
            asset = Bucket()
            asset?.entityId = entityId
        }
        
        if let targetDroneString = try values.decodeIfPresent(String.self, forKey: .targetDrone) {
            let entityId = targetDroneString.components(separatedBy: Drone.resourceWithNamespaceString()+"#").last
            targetDrone = Drone()
            targetDrone?.entityId = entityId
        }
    }
    
    public override class func decodeArray(data:Data) -> [Any]? {
        return try? JSONDecoder().decode([FinishBucketDeliveryTransaction].self, from: data)
    }
    
    public override class func decodeSelf(data:Data) -> Any? {
        return try? JSONDecoder().decode(FinishBucketDeliveryTransaction.self, from: data)
    }
    
    public override func isConsistentToPush() -> Bool {
        if  let _ = self.entityId,
            let _ = self.asset,
            let _ = self.targetDrone {
            return true
        }
        return false
    }
}
