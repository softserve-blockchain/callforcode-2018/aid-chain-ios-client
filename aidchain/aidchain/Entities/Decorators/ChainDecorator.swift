//
//  AidChainDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 8/16/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ChainProperty {
    
    var propertyName: String
    var displayName: String?
    var propertyValue: Any?
    var requiresCustomInputAction: Bool = false
    var customAction: ((_ context: Any?)->Void)?
    
    init(propertyName: String) {
        self.propertyName = propertyName
    }
    
}

class ChainDecorator: NSObject {
 
    private var aidChainEntity: ChainEntity!
    public var excludedPropertiesKeys: [String] = []
    
    init(entity: ChainEntity!, excludedKeys:[String]?) {
        aidChainEntity = entity
        excludedPropertiesKeys = excludedKeys ?? []
    }
    
    private lazy var customProperties:[ChainProperty] = {
        var customProperties = [ChainProperty]()
        for propertyName in targetProperties {
            let customProperty = ChainProperty(propertyName: propertyName)
            customProperty.propertyValue = targetPropertiesDictionarty[propertyName]
            customProperty.displayName = displayNameDictionary()?[propertyName]
            customProperties.append(customProperty)
        }
        
        return customProperties
    }()
    
    public func getChainProperties()-> [ChainProperty] {
        // override this to provide custom props behavior
        return customProperties
    }
    
    public func updateEntity(dictionary:[String:Any?] )-> ChainEntity? {
        for key in dictionary.keys {
            if let value = dictionary[key] {
                targetPropertiesDictionarty[key] = value
            }
        }
        guard let jsonData = try? JSONSerialization.data(withJSONObject: targetPropertiesDictionarty, options: .prettyPrinted) else {return nil}
        
        guard let asset = type(of: aidChainEntity).decodeSelf(data:jsonData) as? ChainEntity else {return nil}
        
        return asset
    }
    
    private lazy var targetPropertiesDictionarty: [String:Any]  = {
        do {
            var data = try JSONEncoder().encode(aidChainEntity)
            let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            return dictionary!
        }
        catch {
            
        }
        return [String:Any]()
    }()
    
    private lazy var targetProperties : [String] = {
        let res = Array(Set(Array(self.targetPropertiesDictionarty.keys)).subtracting(Set(excludedPropertiesKeys)))
        return res
    }()

    internal func displayNameDictionary()->[String:String]? {
        return nil
    }
}
