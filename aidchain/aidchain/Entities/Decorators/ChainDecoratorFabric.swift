//
//  ChainDecoratorFabric.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ChainDecoratorFabric {

    public class func createDecorator(entity: ChainEntity!, excludedKeys:[String]?)-> ChainDecorator? {
        if let assetType = typeToAsset(type: type(of: entity)) {
            switch assetType {
            case .AidSeeker:
                return SeekerDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidDonor:
                return DonorDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidFund:
                return FundDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidDrone:
                return DroneDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidInventory:
                return InventoryDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidExchangeProcessor:
                return ExchangeProcessorDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidCommodity:
                return CommodityDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidBucket:
                return BucketDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidRequest:
                return RequestDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidOpenDonateTransaction:
                return OpenDonateTxDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidApproveRequestTransaction:
                return ApproveRequestTxDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidDonateTransaction:
                return DonateTxDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidPurchaseBucketTransaction:
                return PurchaseBucketTxDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidInventoryBucketRegistrationTransaction:
                return InventoryBucketRegTxDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidStartBucketDeliveryTransaction:
                return StartBucketDeliveryTxDecorator(entity: entity,excludedKeys: excludedKeys)
            case .AidFinishBucketDeliveryTransaction:
                return FinishBucketDeliveryTxDecorator(entity: entity,excludedKeys: excludedKeys)
            }
        }
        return ChainDecorator(entity: entity,excludedKeys: excludedKeys)
    }
    
}
