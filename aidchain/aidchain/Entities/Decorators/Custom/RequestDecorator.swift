//
//  RequestDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/6/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class RequestDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [Request.CodingKeys.aiddescription.rawValue:NSLocalizedString("Request description", comment: ""),
                Request.CodingKeys.requestStatus.rawValue:NSLocalizedString("Request status", comment: ""),
                Request.CodingKeys.donors.rawValue:NSLocalizedString("Donators", comment: ""),
                Request.CodingKeys.seeker.rawValue:NSLocalizedString("Aid seeker", comment: ""),
                Request.CodingKeys.approver.rawValue:NSLocalizedString("Approver fund", comment: ""),
                Request.CodingKeys.requiredBalance.rawValue:NSLocalizedString("Required balance", comment: ""),
                Request.CodingKeys.currentBalance.rawValue:NSLocalizedString("Current balance", comment: ""),
                Request.CodingKeys.aidBuckets.rawValue:NSLocalizedString("Formed buckets", comment: ""),
                Request.CodingKeys.targetLocation.rawValue:NSLocalizedString("Target location place", comment: ""),
                Request.CodingKeys.creationTime.rawValue:NSLocalizedString("Date of creation", comment: ""),
                Request.CodingKeys.deliveryTime.rawValue:NSLocalizedString("Date of delivery", comment: "")]
    }

    public override func getChainProperties()-> [ChainProperty] {
        // override this to provide custom props behavior
        let props = super.getChainProperties()
        
        
        for chainproperty in props {
           if chainproperty.propertyName == "targetLocation" {
            chainproperty.requiresCustomInputAction = true
            chainproperty.customAction = { sender in
                let vc = MapSelectionController(nibName: "MapSelectionController", bundle: nil)
                vc.completionHandler = { location in
                    chainproperty.propertyValue = "\(location.latitude);\(location.longitude)"
                    
                    if let nav = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                        nav.popViewController(animated: true)
                    }
                    if let sender = sender as? UIButton {
                        sender.setTitle("\(String(format: "%.2f", location.latitude));\(String(format: "%.2f", location.longitude))", for: .normal)
                    }
                }
                if let nav = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    nav.pushViewController(vc, animated: true)
                }
            }
            }
        }
        return props
    }
    
}
