//
//  InventoryDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class InventoryDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        return [Inventory.CodingKeys.entityId.rawValue:NSLocalizedString("Inventory identifier", comment: ""),
                Inventory.CodingKeys.locationAddress.rawValue:NSLocalizedString("Inventory location address", comment: ""),
                Inventory.CodingKeys.buckets.rawValue:NSLocalizedString("Buckets", comment: "")]
    }
}
