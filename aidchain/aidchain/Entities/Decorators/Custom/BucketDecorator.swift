//
//  BucketDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class BucketDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [Bucket.CodingKeys.aidRequest.rawValue:NSLocalizedString("Aid request", comment: ""),
                Bucket.CodingKeys.commodities.rawValue:NSLocalizedString("Commodities", comment: ""),
                Bucket.CodingKeys.bucketStatus.rawValue:NSLocalizedString("Bucket status", comment: "")]
    }
}
