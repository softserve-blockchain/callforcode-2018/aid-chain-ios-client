//
//  FinishBucketDeliveryTxDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class FinishBucketDeliveryTxDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [FinishBucketDeliveryTransaction.CodingKeys.entityId.rawValue:NSLocalizedString("Finish delivery transaction identifier", comment: ""),
                FinishBucketDeliveryTransaction.CodingKeys.timestamp.rawValue:NSLocalizedString("Creation timestamp", comment: ""),
                FinishBucketDeliveryTransaction.CodingKeys.asset.rawValue:NSLocalizedString("Delivered bucket", comment: ""),
                FinishBucketDeliveryTransaction.CodingKeys.targetDrone.rawValue:NSLocalizedString("Target drone", comment: "")]
    }
}
