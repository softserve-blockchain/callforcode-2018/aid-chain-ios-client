//
//  FundDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class FundDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        return [Fund.CodingKeys.entityId.rawValue:NSLocalizedString("Fund identifier", comment: ""),
                Fund.CodingKeys.walletAddress.rawValue:NSLocalizedString("Wallet address", comment: ""),
                Fund.CodingKeys.balance.rawValue:NSLocalizedString("Balance", comment: ""),
                Fund.CodingKeys.approvedRequests.rawValue:NSLocalizedString("Approved requests", comment: ""),
                Fund.CodingKeys.fundDescription.rawValue:NSLocalizedString("Description", comment: "")]
    }
}
