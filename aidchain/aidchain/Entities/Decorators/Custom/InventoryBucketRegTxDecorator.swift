//
//  InventoryBucketRegTxDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class InventoryBucketRegTxDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [InventoryBucketRegistrationTransaction.CodingKeys.entityId.rawValue:NSLocalizedString("Inventory registration transaction identifier", comment: ""),
                InventoryBucketRegistrationTransaction.CodingKeys.timestamp.rawValue:NSLocalizedString("Creation timestamp", comment: ""),
                InventoryBucketRegistrationTransaction.CodingKeys.asset.rawValue:NSLocalizedString("Bucket to register", comment: ""),
                InventoryBucketRegistrationTransaction.CodingKeys.targetInventory.rawValue:NSLocalizedString("Target inventory", comment: "")]
    }
}
