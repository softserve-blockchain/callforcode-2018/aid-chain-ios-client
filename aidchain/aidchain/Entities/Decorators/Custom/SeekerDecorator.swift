//
//  SeekerDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class SeekerDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [Seeker.CodingKeys.entityId.rawValue:NSLocalizedString("Seeker identifier", comment: ""),
                Seeker.CodingKeys.firstName.rawValue:NSLocalizedString("First name", comment: ""),
                Seeker.CodingKeys.lastName.rawValue:NSLocalizedString("Last name", comment: ""),
                Seeker.CodingKeys.requests.rawValue:NSLocalizedString("Requests", comment: "")]
    }
}
