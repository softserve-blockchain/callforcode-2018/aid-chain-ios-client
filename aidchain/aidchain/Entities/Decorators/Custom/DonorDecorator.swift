//
//  DonorDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class DonorDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [Donor.CodingKeys.entityId.rawValue:NSLocalizedString("Donor identifier", comment: ""),
                Donor.CodingKeys.walletAddress.rawValue:NSLocalizedString("Wallet address", comment: ""),
                Donor.CodingKeys.balance.rawValue:NSLocalizedString("Balance", comment: ""),
                Donor.CodingKeys.firstName.rawValue:NSLocalizedString("First name", comment: ""),
                Donor.CodingKeys.lastName.rawValue:NSLocalizedString("Last name", comment: "")]
    }
}
