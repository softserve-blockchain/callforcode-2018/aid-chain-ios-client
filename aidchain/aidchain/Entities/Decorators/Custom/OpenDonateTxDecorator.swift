//
//  OpenDonateTxDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class OpenDonateTxDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        return [OpenDonateTransaction.CodingKeys.entityId.rawValue:NSLocalizedString("Open donate transaction identifier", comment: ""),
                OpenDonateTransaction.CodingKeys.timestamp.rawValue:NSLocalizedString("Creation timestamp", comment: ""),
                OpenDonateTransaction.CodingKeys.asset.rawValue:NSLocalizedString("Transaction asset", comment: ""),
                OpenDonateTransaction.CodingKeys.initiator.rawValue:NSLocalizedString("Transaction initiator", comment: "")]
    }
}
