//
//  ApproveRequestTxDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ApproveRequestTxDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [ApproveRequestTransaction.CodingKeys.entityId.rawValue:NSLocalizedString("Approve request transaction identifier", comment: ""),
                ApproveRequestTransaction.CodingKeys.timestamp.rawValue:NSLocalizedString("Creation timestamp", comment: ""),
                ApproveRequestTransaction.CodingKeys.asset.rawValue:NSLocalizedString("Transaction asset", comment: ""),
                ApproveRequestTransaction.CodingKeys.approver.rawValue:NSLocalizedString("Transaction approver", comment: "")]
    }
}
