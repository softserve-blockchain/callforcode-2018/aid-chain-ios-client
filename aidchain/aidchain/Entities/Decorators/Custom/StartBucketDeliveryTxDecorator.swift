//
//  StartBucketDeliveryTxDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class StartBucketDeliveryTxDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        return [StartBucketDeliveryTransaction.CodingKeys.entityId.rawValue:NSLocalizedString("Start delivery transaction identifier", comment: ""),
                StartBucketDeliveryTransaction.CodingKeys.timestamp.rawValue:NSLocalizedString("Creation timestamp", comment: ""),
                StartBucketDeliveryTransaction.CodingKeys.asset.rawValue:NSLocalizedString("Bucket to deliver", comment: ""),
                StartBucketDeliveryTransaction.CodingKeys.targetDrone.rawValue:NSLocalizedString("Target drone", comment: "")]
    }
}
