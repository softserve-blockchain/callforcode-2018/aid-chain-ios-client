//
//  DroneDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class DroneDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        return [Drone.CodingKeys.entityId.rawValue:NSLocalizedString("Drone identifier", comment: ""),
                Drone.CodingKeys.modelType.rawValue:NSLocalizedString("Model type", comment: ""),
                Drone.CodingKeys.targetBucket.rawValue:NSLocalizedString("Target bucket to deliver", comment: ""),
                Drone.CodingKeys.recentLocation.rawValue:NSLocalizedString("Recent drone location", comment: "")]
    }
}
