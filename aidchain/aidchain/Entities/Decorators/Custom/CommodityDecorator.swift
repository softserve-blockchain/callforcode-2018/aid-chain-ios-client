//
//  CommodityDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class CommodityDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        
        return [Commodity.CodingKeys.commodityType.rawValue:NSLocalizedString("Commodity type", comment: ""),
                Commodity.CodingKeys.commodityDescription.rawValue:NSLocalizedString("Commodity description", comment: ""),
                Commodity.CodingKeys.purchasePrice.rawValue:NSLocalizedString("Purchase price", comment: ""),
                Commodity.CodingKeys.weightKg.rawValue:NSLocalizedString("Weight (kg)", comment: "")]
    }
}
