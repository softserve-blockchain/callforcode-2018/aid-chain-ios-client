//
//  PurchaseBucketTxDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class PurchaseBucketTxDecorator: ChainDecorator {
    
    override func displayNameDictionary()->[String:String]? {
        return [PurchaseBucketTransaction.CodingKeys.entityId.rawValue:NSLocalizedString("Purchase bucket transaction identifier", comment: ""),
                PurchaseBucketTransaction.CodingKeys.timestamp.rawValue:NSLocalizedString("Creation timestamp", comment: ""),
                PurchaseBucketTransaction.CodingKeys.asset.rawValue:NSLocalizedString("Bucket to purchase", comment: ""),
                PurchaseBucketTransaction.CodingKeys.processor.rawValue:NSLocalizedString("Processor identifier", comment: "")]
    }
}
