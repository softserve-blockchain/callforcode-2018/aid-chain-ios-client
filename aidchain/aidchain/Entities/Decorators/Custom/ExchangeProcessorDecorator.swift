//
//  ExchangeProcessorDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class ExchangeProcessorDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        return [ExchangeProcessor.CodingKeys.entityId.rawValue:NSLocalizedString("Exchange processor identifier", comment: ""),
                ExchangeProcessor.CodingKeys.bucketsToProcess.rawValue:NSLocalizedString("Buckets to process", comment: "")]
    }
}
