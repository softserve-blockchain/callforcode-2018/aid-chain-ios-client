//
//  DonateTxDecorator.swift
//  aidchain
//
//  Created by Denys Doronin on 9/12/18.
//  Copyright © 2018 SoftServe. All rights reserved.
//

import UIKit

class DonateTxDecorator: ChainDecorator {

    override func displayNameDictionary()->[String:String]? {
        return [DonateTransaction.CodingKeys.entityId.rawValue:NSLocalizedString("Donate transaction identifier", comment: ""),
                DonateTransaction.CodingKeys.timestamp.rawValue:NSLocalizedString("Creation timestamp", comment: ""),
                DonateTransaction.CodingKeys.destination.rawValue:NSLocalizedString("Destination fund", comment: ""),
                DonateTransaction.CodingKeys.donor.rawValue:NSLocalizedString("Donor identifier", comment: ""),
                DonateTransaction.CodingKeys.amount.rawValue:NSLocalizedString("Amount", comment: "")]
    }
}
