# Aidchain iOS 

## Now it AppStore! Please checkout this [link](https://itunes.apple.com/us/app/aidchain/id1436167077?ls=1&mt=8)

Mitigating natural disasters is one of the world’s greatest challenges.

The past decade has been one of the worst periods for natural disasters and while they may be inevitable they don’t have to become catastrophic.
Supply networks built on blockchain technology improve the distribution of aid before and after a major disaster.
Aidchain application specifically targeted to decrease the impact of any disaster and provide maximum transparency leveraging full benefits of blockchain - immutability, consensus, and provenance.

The solution combines different people into one single verifiable network - 
1. Submit a request when You or someone You know may need help;
2. A charity organization approves the request and purchases required equipment like medicine, first aid kit or any required stuff to save a life.
3. The last but not least step is aid delivery by drones. Sometimes the only way to achieve the place with people searching for help is air.  

Actual delivery is limited due to drone capacity and battery but the main benefit is to deliver aid where it's not possible by car or human bean.

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-ios-client/uploads/6057932cad9540cae823cb71e8ca71da/main.png)

iOS application allows different aidchain roles to operate with blockchain. Let's dive deeper into particular roles:

1. Aid seeker - a person who creates aid request into the system. 
2. Donor - a person who donates money (via fiat or cryptocurrencies) into particular fund
3. Fund - a charity organization which holds donates
4. Invetory - a place where all purchased items are stored to be delivered
5. Drone - a drone which actually delivers goodies to particular place
6. Exchange processor - internal component which provides exchange capabilities

## Use cases

For better understanding when and how solution is used and how it helps to mitigate disaster here is main high-level use cases.

Participants from disaster areas submit aid delivery requests for medicine, comm. gadgets, food etc.

Aid requests are split into buckets and loaded into drones at inventory locations.

Participants donate via fiat or crypto currencies. The donations are stored on a pubic charity accounts. Smart contract controls how money is withdrawn to complete request.

When target bucket is purchased its delivery is tracked via IOT sensors and recorded into blockchain. Request is automatically  closed when all buckets are processed.

All transactions, submissions and request changes are tracked via blockchain to provide ultimate level of visability.


## 1. Create request flow

If we're talking about the request submition flow in details, let's assume that user (aid seeker) has already been registered in application.
Aid seeker logins into application and choose aid request option from the main menu. After it, list of previously created requests appear and option to create a new one. User should input some basic information what is needed and select a place on the map where this aid should be delivered.
After all this done, user confirms request by sending transaction to blockchain to register request for further processing.

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-ios-client/uploads/c6356dc7c594e418c5e2a5d81d712eea/background_2.png)

## 2. Fund approve flow

After request is opened fund may approve this request by reviewing all pending submitions. 
Approving request means that goodies will be purchased from fund's account.
So, fund logins into system, selects from the main menu approve request option and sends transaction into blockchain confirming that money should be withdrawn from account.

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-ios-client/uploads/36ebc6b9cdf58042b3e21bacf2c51fda/background.png)

## 3. Invetory bucket registration flow

All pending requests requires decomposition into buckets due to limited drone capacity. 
Inventory is responsible for creating buckets from seeker's description. 
After request approval buckets would be purchased by exchange processor and delivered to invetory for further processing. 
To track delivery inventory registers bucket when it goodies appears in it, so it could be then handled by drones. 

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-ios-client/uploads/87db91cfc1339dd9378d4bd5a14874bc/background_4.png)

## 4. Drone delivery flow

Aid is packed into bucket and sent by drones to specific location.
Drone’s position is tracked, data is sent via IOT sensor back to system to track delivery.
Once drone reaches target position and delivers bucket smart contract automatically closes current request and marked it as done.

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-ios-client/uploads/4bd7d34b7e9e3917d3da3916529f7856/background_3.png)

# Aidchain team:

Andriy Shapochka ashapoch@softserveinc.com

Dmytro Ovcharenko dovchar@softserveinc.com

Denys Doronin ddor@softserveinc.com
